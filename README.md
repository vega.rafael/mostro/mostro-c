## MOSTRO-C

This project uses the heavy hvcc compiler to convert pd patches into C files and compiles the C files into a linux alsa application to implement an audio synthesizer.

## A development workflow using virtual box.

1. Burn the devel image to a micro SD card, boot the MOSTRO from it. Note that this image will not burn itself to the internal MMC memory of the MOSTRO. This means you need to keep the SD card in the MOSTRO for as long as you want to run the devel version of the firmware. Image is [here](https://gitlab.com/outer-space-sounds/mostro/linux-filesystem/-/tree/3.0/releases/3.0.devel).

2. Download virtual machine from [here](https://mega.nz/file/xmhn1T4I#vpH_SSFFOwJE360eSwsa6ffFYzeyZg5F68HebZTKePM) (big download, could have been much worse). I recommend you download using the mega.nz desktop app from [here](https://mega.nz/sync).

3. Watch these videos: [Video 1](https://mega.nz/file/BjQ13CYA#KHQoPbkXN-rPmjNsKyP8t8rbaH1FV1WCUt0fpDwNwdo), [Video 2](https://mega.nz/file/V6J1hIZT#Jzgt0LgjGWyA-PYpz7EFDM035kj7YVh1utHaFMCTeqM), [Video 3](https://mega.nz/file/d2oWEKIY#2q4t0LEnI3n4OW6x3ziJ6qgbNfSadEjzMm2tXZL9cfI).

4. Make that MOSTRO sing!


## Building for Linux x86 and Linux ARM (Beaglebone)

Build time requirements: 
* python2.7
* python2-enum
* python2-Jinja2 
* meson

Run time requirements: 
* Linux
* libld
* A properly configured ALSA sound system

Compile using the following commands:

```
cd path/to/this/repo
meson build --buildtype=[release|debug] [--cross-file=scripts/cross-compile-arm.ini]
cd build
ninja  
ninja generate_code # Not re-triggered automatically when patches change
```

Compiling for linux ARM uses the compiler, linker and c library in the mostro/linux-filesystem repo. The compiler binaries in that repo should be in your build machine PATH.

### Running on the Beaglebone.

A practical workflow is to boot the MOSTRO using a development image that can be created using the scripts in the mostro/linux. Once you do that, connect to the MOSTRO via SSH (look at the mostro/pd repo readme for more instructions) and do something like

```
cd build
ninja install_remote
ssh root@192.168.7.2
cd /root/mostro-c
./synth &
./priorities.sh
```

Note that the priorities.sh script set the process and irq priorities to ensure better real time performance. (as much as can be achieved with linux).

### Debugging remotely

Read [this](https://buildroot.org/downloads/manual/manual.html#_using_literal_gdb_literal_in_buildroot).

On the beagle, start the synth with gdb server:

```
ssh root@192.168.7.2
cd /root/mostro-c
gdbserver :1234 synth
```

On the build machine:

```
cd build
../../linux-filesystem/buildroot/output/host/bin/arm-buildroot-linux-uclibcgnueabihf-gdb -x ../../linux-filesystem/buildroot/output/host/arm-buildroot-linux-uclibcgnueabihf/sysroot/usr/share/buildroot/gdbinit synth
target remote 192.168.7.2:1234
```

## Git Tags

Git tags called pd_patch_1, pd_patch_2, pd_patch_<n>, point to versions of the patch and generated c/c++ code that are considered "good". 

## License

Copyright (C) 2015 Rafael Vega <contacto@rafaelvega.co>

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.  

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.


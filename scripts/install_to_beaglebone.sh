#! /bin/bash

REMOTE_HOST="root@192.168.7.2"
REMOTE_PATH="/root/mostro-c"
BINARY=build/synth
PATCH=patches
ASSETS=assets

ssh ${REMOTE_HOST} "killall synth; mkdir -p ${REMOTE_PATH}"
rsync -avu $BINARY ${REMOTE_HOST}:${REMOTE_PATH}
rsync -avu $PATCH ${REMOTE_HOST}:${REMOTE_PATH}
rsync -avu $ASSETS ${REMOTE_HOST}:${REMOTE_PATH}
rsync -avu scripts/priorities.sh ${REMOTE_HOST}:${REMOTE_PATH}

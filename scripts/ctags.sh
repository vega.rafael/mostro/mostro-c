#!/bin/sh

set -e
PATH="/usr/local/bin:$PATH"
trap 'rm -f "$$.tags"' EXIT
rm -f files
find ./vendors/the-sound-void/include -type f >> files
find ./vendors/the-sound-void/src -type f >> files
find ./src -type f >> files
find ../pru/library -type f >> files
ctags --tag-relative -L files -f"$$.tags" --languages=c,c++
rm files
mv "$$.tags" "tags"

#! /bin/sh

# Here, we're setting the scheduling priority for IRQ threads. This will 
# reduce overall Audio and USB (midi) latency. 
# Read the following:
# http://subversion.ffado.org/wiki/IrqPriorities
# https://wiki.linuxaudio.org/wiki/lowlatency_deprecated
#
# These are the priorities we want:
#
# IRQ            Priority
# rtc0           99
# rtc0           99
# edma_ccint     98
# dma_controller 98
# mcasp_tx       97
# mcasp_rx       97 
# musb-hdrc.0    96
# musb-hdrc.1    96
# 
# Process (not irq)   priority
# ksoftirqd           95
# pd                  92 (set automatically when launching pd)


# The general procedure is to look for a IRQ name in /proc/interrupts, 
# get the IRQ number, get the process id for the IRQ handler thread with ps;
# and set the process priority with chrt.

set_irq_priority() 
{
  IRQ_NAME=$1
  PRIORITY=$2

  IRQ_ID=`cat /proc/interrupts | grep ${IRQ_NAME} | cut -d':' -f0`

  for I in $IRQ_ID
  do
    PID=`ps -o pid,comm | grep "irq/${I}"`
    # echo $PID
    chrt -f -p ${PRIORITY} ${PID} > /dev/null
  done

  # chrt -f -p ${PRIORITY} ${PID} > /dev/null
}

set_process_priority()
{
  PROCESS_NAME=$1
  PRIORITY=$2

  PID=`ps -o pid,comm | grep "${PROCESS_NAME}" | cut -d'k' -f1`
  chrt -f -p ${PRIORITY} ${PID} > /dev/null
}

# # Reset all irq priorities to 50
# PIDS=`ps -o pid,comm | egrep -i "irq/[0-9]+" | awk '{print $1}'`
# for PID in $PIDS
# do
#   chrt -p -f 50 ${PID}
# done
  
set_irq_priority "rtc0" 99
set_irq_priority "edma_ccint" 98
set_irq_priority "dma-controller" 98
set_irq_priority "mcasp_tx" 97
set_irq_priority "mcasp_rx" 97 
set_irq_priority "musb-hdrc.0" 96
set_irq_priority "musb-hdrc.1" 96
set_process_priority "ksoftirqd" 95
set_process_priority "synth" 92



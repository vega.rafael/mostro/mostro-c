#! /bin/bash

# exit when any command fails
set -e

rm -rf src/generated/c/*

# Generate c code from pd patch
python2 vendors/hvcc/hvcc.py patches/main.pd -o src/generated -n synth -g c --copyright "Copyright (c) Outer Space Sounds 2020"

# The following lines include pragma commands in the generated code to avoid compiler warnings.
# This does NOT fix the warnings, just silences them

for f in src/generated/c/*.h*; do
  echo 'Generated' $f
  sed -i '1s;^;#pragma GCC system_header\n;' $f
done

for f in src/generated/c/*.c*; do
  echo 'Generated' $f
  sed -i '1s;^;#pragma GCC diagnostic ignored "-Wunused-parameter"\n;' $f
  sed -i '1s;^;#pragma GCC diagnostic ignored "-Wsign-compare"\n;' $f
  sed -i '1s;^;#pragma GCC diagnostic ignored "-Wimplicit-fallthrough="\n;' $f
  sed -i '1s;^;#pragma GCC diagnostic ignored "-Wstringop-overflow"\n;' $f
  sed -i '1s;^;#pragma GCC diagnostic ignored "-Wstrict-aliasing"\n;' $f
  sed -i '1s;^;#pragma GCC diagnostic ignored "-Wstrict-aliasing"\n;' $f

  sed -i '/#pragma mark/d' $f
done

exit 0

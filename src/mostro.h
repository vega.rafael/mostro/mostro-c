#ifndef MOSTRO_H
#define MOSTRO_H

#include "soundvoid/poly.h"

#include "audio_io.h"
#include "buttons_knobs.h"
#include "scales.h"
#include "synth.h"
#include "leds.h"

#define NUM_LAYERS 1

struct layer_params_t {
  uint octave;
  uint tuning;

  float index;
  float ratio;
  float feedback;
  float envelope_ammount;
  float attack;
  float release;
  float lfo_ammount;
  float lfo_freq;
  float glide;

  uint envelope_destination;
  uint lfo_destination;

  bool lfo_sync;
  bool sub;
  bool amp;
  bool sustain;
};
typedef struct layer_params_t layer_params_t;

struct general_params_t {
  float volume;
  float delay_ammount;
  float delay_time;
  float delay_feedback;
};
typedef struct general_params_t general_params_t;

// This struct holds the general state of the app, pass around a pointer to this.
struct mostro {
  double time;
  synth_t synth;
  buttons_t buttons;
  scales_t scales;
  sv_poly poly;
  leds_t leds;
  general_params_t general_params;
  layer_params_t layer_params[NUM_LAYERS];
};
typedef struct mostro mostro;

void mostro_init(mostro* x);

void mostro_start_note_key(mostro* m, uint key);
void mostro_end_note_key(mostro* m, uint key);

void mostro_set_tuning(mostro* m, uint layer, uint tuning);
void mostro_set_btn_octave_plus(mostro* m);
void mostro_set_btn_octave_minus(mostro* m);

void mostro_set_pot_index(mostro* m, float f);
void mostro_set_pot_ratio(mostro* m, float f);
void mostro_set_pot_feedback(mostro* m, float f);
void mostro_set_pot_env_amt(mostro* m, float f);
void mostro_set_pot_attack(mostro* m, float f);
void mostro_set_pot_release(mostro* m, float f);
void mostro_set_pot_lfo_amt(mostro* m, float f);
void mostro_set_pot_lfo_freq(mostro* m, float f);
void mostro_set_pot_glide(mostro* m, float f);
void mostro_set_pot_volume(mostro* m, float f);
void mostro_set_pot_delay_ammount(mostro* m, float f);
void mostro_set_pot_delay_feedback(mostro* m, float f);
void mostro_set_delay_time(mostro* m, float millis);

void mostro_set_btn_sub(mostro* m);
void mostro_set_btn_env_dest(mostro* m);
void mostro_set_btn_amp(mostro* m);
void mostro_set_btn_sustain(mostro* m);
void mostro_set_btn_lfo_dest(mostro* m);

#endif /* MOSTRO */

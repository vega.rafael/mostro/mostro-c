#ifndef BUTTONS_H
#define BUTTONS_H

#include <beaglebone_pruio_pins.h>
#include <soundvoid/time.h>
#include <stdbool.h>

#include "basics.h"

typedef struct mostro mostro;

#define BUTTONS_MAX_TIMES 4
#define BUTTONS_TAP_HOLD_TIME 0.3
#define BUTTONS_TAP_TIMEOUT 3.0

#define BUTTONS_NUM_KEYS 13

struct buttons_t {
  bool first_time;

  bool tap_is_down;
  bool tap_is_tapping;
  double tap_times[BUTTONS_MAX_TIMES];
  uint n_tap_times;
};
typedef struct buttons_t buttons_t;

int buttons_init(mostro* m);
int buttons_destroy(mostro* m);
int buttons_process(mostro* m);

#endif /* BUTTONS */

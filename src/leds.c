#include <beaglebone_pruio.h>
#include <stdio.h>

#include "basics.h"
#include "leds.h"

int leds_init(mostro* m) {
  (void)(m);

  int all_leds[] = {LED_SUB,       LED_AMP,          LED_SUST,         LED_LOOP,
                    LED_ENV_INDEX, LED_ENV_RATIO,    LED_ENV_FEEDBACK, LED_LFO_INDEX,
                    LED_LFO_RATIO, LED_LFO_FEEDBACK, LED_LFO_PITCH};

  for (uint i = 0; i < sizeof(all_leds) / sizeof(int); ++i) {
    int pin = all_leds[i];
    if (beaglebone_pruio_init_gpio_pin(pin, BEAGLEBONE_PRUIO_GPIO_MODE_OUTPUT)) {
      fprintf(stderr, "Could not initialize pin %i\n", pin);
      return 1;
    }
    led_off(pin);
  }
  return 0;
}

void led_on(int led) {
  beaglebone_pruio_set_pin_value(led, 1);
}

void led_off(int led) {
  beaglebone_pruio_set_pin_value(led, 0);
}

void led_set(int led, int on) {
  beaglebone_pruio_set_pin_value(led, on);
}

#ifndef SYNTH_H
#define SYNTH_H

#include "basics.h"
#include "generated/c/Heavy_synth.h"

#define MAX_VOICES 1

typedef struct mostro mostro;

struct synth_t {
  HeavyContextInterface* heavy;
  uint sample_rate;
};
typedef struct synth_t synth_t;

int synth_init(mostro* m);
void synth_destroy(mostro* m);

int synth_process(mostro* m, float* buffer, uint n);

void synth_set_pitch(mostro* m, int voice, float pitch);
void synth_set_velocity(mostro* m, int voice, int velocity);

void synth_set_index(mostro* m, int voice, float f);
void synth_set_ratio(mostro* m, int voice, float f);
void synth_set_feedback(mostro* m, int voice, float f);
void synth_set_env_amt(mostro* m, int voice, float f);
void synth_set_attack(mostro* m, int voice, float f);
void synth_set_release(mostro* m, int voice, float f);
void synth_set_lfo_amt(mostro* m, int voice, float f);
void synth_set_lfo_freq(mostro* m, int voice, float f);
void synth_lfo_reset(mostro* m, int voice);
void synth_set_glide(mostro* m, int voice, float f);
void synth_set_delay_ammount(mostro* m, int voice, float f);
void synth_set_delay_feedback(mostro* m, int voice, float f);
void synth_set_delay_time(mostro* m, int voice, float f);
void synth_set_volume(mostro* m, int voice, float f);

void synth_set_sub(mostro* m, int voice, bool sub);
void synth_set_env_dest_index(mostro* m, int voice, bool value);
void synth_set_env_dest_ratio(mostro* m, int voice, bool value);
void synth_set_env_dest_feedback(mostro* m, int voice, bool value);
void synth_set_amp(mostro* m, int voice, bool value);
void synth_set_sustain(mostro* m, int voice, bool value);
void synth_set_lfo_dest_index(mostro* m, int voice, bool value);
void synth_set_lfo_dest_ratio(mostro* m, int voice, bool value);
void synth_set_lfo_dest_feedback(mostro* m, int voice, bool value);
void synth_set_lfo_dest_pitch(mostro* m, int voice, bool value);
#endif /* SYNTH */

#ifndef BASICS_H
#define BASICS_H

#ifdef __cplusplus
extern "C" {
#endif

/*
 * @brief unsigned integer type
 */
typedef unsigned int uint;

/*
 * @brief unsigned char type
 */
typedef unsigned char uchar;

/*
 * @brief null constant definition
 */
#define null 0

/*
 * @brief print_debug() macro
 */
// clang-format off
#if DEBUG
   #define print_debug(msg, a) printf(msg, a);
   #define print_debug_2(msg, a, b) printf(msg, a, b);
   #define print_debug_3(msg, a, b, c) printf(msg, a, b, c);
#else
   #define print_debug(msg, a) do {} while (0);
   #define print_debug_2(msg, a, b) do {} while (0);
   #define print_debug_3(msg, a, b, c) do {} while (0);
#endif
// clang-format on

#ifdef __cplusplus
}
#endif

#endif /* ifndef BASICS_H */

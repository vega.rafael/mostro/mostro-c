#include <assert.h>
#include <dirent.h>
#include <soundvoid/tuning.h>
#include <string.h>

#include "mostro.h"
#include "scales.h"

// TODO:
// * Check if user_dir/tunings directory em->ists, create it with built-in tunings.
// * Scales
// * Octaves
// * Chord mode, note mode
// * If a tuning file cannot be loaded correctly, use standard tuning.

static int load_tuning_file(char const* restrict filename, float* restrict table) {
  return sv_tuning_parse_file(table, MAX_NOTES, filename);
}

static int load_tuning_files(mostro* m) {
  char* dir_name = "./assets/tunings/";
  DIR* d = opendir(dir_name);
  if (!d) {
    return 0;
  }

  struct dirent* entry = null;
  int n = 0;
  while ((entry = readdir(d)) != null) {
    char* filename = entry->d_name;
    uint len = strlen(filename);
    if (strcmp(&filename[len - 4], ".tun") == 0) {
      char path[256] = "";
      strcat(path, dir_name);
      strcat(path, filename);
      if (!load_tuning_file(path, m->scales.tunings[n])) {
        n++;
      }
    }
  }
  return n;
}

void scales_init(mostro* m) {
  uint n = load_tuning_files(m);
  m->scales.num_tunings = n;

  for (uint i = 0; i < NUM_LAYERS; ++i) {
    mostro_set_tuning(m, i, 0);
  }
}

float scales_note_to_freq(mostro* m, uint note_number) {
  assert(note_number < 128);

  // tuning = m->layer_params[0].tuning;
  float* table = m->scales.tunings[0];
  float freq = table[note_number];
  return freq;
}

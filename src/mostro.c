#include "mostro.h"

#include <limits.h>

void mostro_init(mostro* x) {
  assert(x);

  general_params_t* g = &x->general_params;
  g->volume = 0;
  g->delay_ammount = 0;
  g->delay_time = 0;
  g->delay_feedback = 0;


  for (uint i = 0; i < NUM_LAYERS; ++i) {
    layer_params_t* l = &x->layer_params[i];
    l->octave = 0;
    l->tuning = 0;

    l->sub = 0;
    l->index = 0;
    l->ratio = 0;
    l->feedback = 0;

    l->envelope_destination = 0;
    l->envelope_ammount = 0;
    l->attack = 0;
    l->sustain = false;
    l->release = 0;
    l->amp = false;

    l->lfo_sync = false;
    l->lfo_ammount = 0;
    l->lfo_freq = 0;
    l->lfo_destination = 0;

    l->glide = 0;
  }

  scales_init(x);
  synth_init(x);
  buttons_init(x);
  leds_init(x);

  sv_poly_init(&x->poly);
  sv_poly_set_num_voices(&x->poly, 1);
  sv_poly_set_user_data(&x->poly, x);

  mostro_set_btn_env_dest(x);
  mostro_set_btn_lfo_dest(x);
  mostro_set_btn_sub(x);
  mostro_set_btn_amp(x);
  mostro_set_btn_sustain(x);
}

void mostro_set_tuning(mostro* m, uint layer, uint tuning) {
  m->layer_params[layer].tuning = tuning;
}


////////////////////////////////////////////////////////////////////////////////
// NOTES
//

void sv_poly_cbk_note_off(uint voice, uchar note, uchar velocity, void* user_data) {
  (void)velocity;
  (void)note;
  mostro* m = (mostro*)user_data;
  synth_set_velocity(m, voice, 0);
}

void sv_poly_cbk_note_on(uint voice, uchar note, uchar velocity, void* user_data) {
  mostro* m = (mostro*)user_data;
  bool lfo_sync = m->layer_params[0].lfo_sync;
  if (lfo_sync) {
    synth_lfo_reset(m, voice);
  }

  float freq = scales_note_to_freq(m, note);
  synth_set_pitch(m, voice, freq);
  synth_set_velocity(m, voice, velocity);
}

void mostro_start_note_key(mostro* m, uint key) {
  // TODO: Scales.
  //       Proper octave handling: Store which keys are active, with the note
  //       they trigger. When the key is released, call poly_note_off() with
  //       the appropriate note
  layer_params_t* l = &m->layer_params[0];
  uint note = key + l->octave * 12;
  sv_poly_note_on(&m->poly, note, 100);
}

void mostro_end_note_key(mostro* m, uint key) {
  layer_params_t* l = &m->layer_params[0];
  uint note = key + l->octave * 12;
  sv_poly_note_off(&m->poly, note, 0);
}

////////////////////////////////////////////////////////////////////////////////
// POTENTIOMETERS

// TODO: pickup mode
void mostro_set_pot_index(mostro* m, float f) {
  layer_params_t* l = &m->layer_params[0];
  l->index = f;
  synth_set_index(m, 0, f);
}

void mostro_set_pot_ratio(mostro* m, float f) {
  layer_params_t* l = &m->layer_params[0];
  l->ratio = f;
  synth_set_ratio(m, 0, f);
}

void mostro_set_pot_feedback(mostro* m, float f) {
  layer_params_t* l = &m->layer_params[0];
  l->feedback = f;
  synth_set_feedback(m, 0, f);
}

void mostro_set_pot_env_amt(mostro* m, float f) {
  layer_params_t* l = &m->layer_params[0];
  l->envelope_ammount = f;
  synth_set_env_amt(m, 0, f);
}

void mostro_set_pot_attack(mostro* m, float f) {
  layer_params_t* l = &m->layer_params[0];
  l->attack = f;
  synth_set_attack(m, 0, f);
}

void mostro_set_pot_release(mostro* m, float f) {
  layer_params_t* l = &m->layer_params[0];
  l->release = f;
  synth_set_release(m, 0, f);
}

void mostro_set_pot_lfo_amt(mostro* m, float f) {
  float low = 0.48;
  float hi = 0.52;
  bool sync = false;
  float val = 0;


  if (f <= low) {
    val = (low - f) / low;
    sync = false;
  }
  else if (f > low && f < hi) {
    val = 0;
    sync = false;
  }
  else { // >= hi
    val = (f - hi) / (1.0 - hi);
    sync = true;
  }

  print_debug_3("lfo amt f:%f v:%f s:%i\n", f, val, sync);

  layer_params_t* l = &m->layer_params[0];
  l->lfo_ammount = f;
  l->lfo_sync = sync;
  synth_set_lfo_amt(m, 0, val);
}

void mostro_set_pot_lfo_freq(mostro* m, float f) {
  layer_params_t* l = &m->layer_params[0];
  l->lfo_freq = f;
  synth_set_lfo_freq(m, 0, f);
}

void mostro_set_pot_glide(mostro* m, float f) {
  layer_params_t* l = &m->layer_params[0];
  l->glide = f;
  synth_set_glide(m, 0, f);
}

void mostro_set_delay_time(mostro* m, float millis) {
  m->general_params.delay_time = millis;
  synth_set_delay_time(m, 0, millis);
}

void mostro_set_pot_delay_feedback(mostro* m, float f) {
  m->general_params.delay_feedback = f;
  synth_set_delay_feedback(m, 0, f);
}

void mostro_set_pot_delay_ammount(mostro* m, float f) {
  general_params_t* g = &m->general_params;
  g->delay_ammount = f;
  synth_set_delay_ammount(m, 0, f);
}

void mostro_set_pot_volume(mostro* m, float f) {
  general_params_t* g = &m->general_params;
  g->volume = f;
  synth_set_volume(m, 0, f);
}

////////////////////////////////////////////////////////////////////////////////
// BUTTONS
//

void mostro_set_btn_octave_plus(mostro* m) {
  layer_params_t* l = &m->layer_params[0];
  uint octave = l->octave;
  if (octave == 8) {
    octave = 1;
  }
  else {
    octave++;
  }
  l->octave = octave;
}

void mostro_set_btn_octave_minus(mostro* m) {
  layer_params_t* l = &m->layer_params[0];
  uint octave = l->octave;
  if (octave == 1) {
    octave = 8;
  }
  else {
    octave--;
  }
  l->octave = octave;
}

void mostro_set_btn_sub(mostro* m) {
  layer_params_t* l = &m->layer_params[0];
  bool sub = !l->sub;
  l->sub = sub;
  synth_set_sub(m, 0, sub);
  led_set(LED_SUB, sub);
}

void mostro_set_btn_env_dest(mostro* m) {
  // mostro->layer_parameters->envelope_destination is an unsigned integer.
  // Bit 2 represents envelope_2_index.
  // Bit 1 represents envelope_2_ratio.
  // Bit 0 represents envelope_2_feedback.
  // The order[] array shows the order in which the envelope_destination integer must be incremented

  uint order[] = {0x4, 0x2, 0x1, 0x6, 0x5, 0x3, 0x7};

  layer_params_t* l = &m->layer_params[0];
  uint dest = l->envelope_destination;

  // Find current value in order array.
  uint i = UINT_MAX;
  for (uint j = 0; j < 7; ++j) {
    if (order[j] == dest) {
      i = j;
      break;
    }
  }

  // Increment
  i++;
  if (i >= 7) {
    i = 0;
  }
  dest = order[i];
  l->envelope_destination = dest;

  bool index = dest & 4;
  bool ratio = dest & 2;
  bool feedback = dest & 1;
  synth_set_env_dest_index(m, 0, index);
  synth_set_env_dest_ratio(m, 0, ratio);
  synth_set_env_dest_feedback(m, 0, feedback);
  led_set(LED_ENV_INDEX, index);
  led_set(LED_ENV_RATIO, ratio);
  led_set(LED_ENV_FEEDBACK, feedback);
}

void mostro_set_btn_amp(mostro* m) {
  layer_params_t* l = &m->layer_params[0];
  bool amp = !l->amp;
  l->amp = amp;
  synth_set_amp(m, 0, amp);
  led_set(LED_AMP, amp);
}

void mostro_set_btn_sustain(mostro* m) {
  layer_params_t* l = &m->layer_params[0];
  bool sustain = !l->sustain;
  l->sustain = sustain;
  synth_set_sustain(m, 0, sustain);
  led_set(LED_SUST, sustain);
}

void mostro_set_btn_lfo_dest(mostro* m) {
  // mostro->layer_parameters->lfo_destination is an unsigned integer.
  // Bit 3 represents lfo_2_index.
  // Bit 2 represents lfo_2_ratio.
  // Bit 1 represents lfo_2_feedback.
  // Bit 0 represents lfo_2_feedback.
  // The order[] array shows the order in which the lfo_destination integer must be incremented

  uint order[] = {8, 4, 2, 1, 12, 10, 9, 6, 5, 3, 14, 13, 11, 7, 15};
  layer_params_t* l = &m->layer_params[0];
  uint dest = l->lfo_destination;

  // Find current value in order array.
  uint i = UINT_MAX;
  for (uint j = 0; j < 15; ++j) {
    if (order[j] == dest) {
      i = j;
      break;
    }
  }

  // Increment
  i++;
  if (i >= 15) {
    i = 0;
  }
  dest = order[i];
  l->lfo_destination = dest;

  bool index = dest & 8;
  bool ratio = dest & 4;
  bool feedback = dest & 2;
  bool pitch = dest & 1;
  synth_set_lfo_dest_index(m, 0, index);
  synth_set_lfo_dest_ratio(m, 0, ratio);
  synth_set_lfo_dest_feedback(m, 0, feedback);
  synth_set_lfo_dest_pitch(m, 0, pitch);
  led_set(LED_LFO_INDEX, index);
  led_set(LED_LFO_RATIO, ratio);
  led_set(LED_LFO_FEEDBACK, feedback);
  led_set(LED_LFO_PITCH, pitch);
}

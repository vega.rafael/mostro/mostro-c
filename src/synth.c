#include <stdio.h>

#include "mostro.h"
#include "synth.h"

// Macros to concatenate synth parameter tokens.
// See them in action in synth_set_pitch and similar functions below.
#define SYNTH_IF(p, P, v, i)                                                                       \
  do {                                                                                             \
    if (v == i) {                                                                                  \
      p = P##_##i;                                                                                 \
    }                                                                                              \
  } while (0)

#define SYNTH_P_V(p, P, v)                                                                         \
  assert(v >= 0 || v < MAX_VOICES);                                                                \
  Hv_synth_ParameterIn param;                                                                      \
  SYNTH_IF(p, P, v, 0);

/*
// SYNTH_IF(p, P, v, 1);                                                                    \
// SYNTH_IF(p, P, v, 2);                                                                    \
// SYNTH_IF(p, P, v, 3);                                                                    \
// SYNTH_IF(p, P, v, 4);                                                                    \
// SYNTH_IF(p, P, v, 5);                                                                    \
// SYNTH_IF(p, P, v, 6);                                                                    \
// SYNTH_IF(p, P, v, 7);                                                                    \
// SYNTH_IF(p, P, v, 8);                                                                    \
// SYNTH_IF(p, P, v, 9);                                                                    \
// SYNTH_IF(p, P, v, 10);                                                                   \
// SYNTH_IF(p, P, v, 11);                                                                   \
// SYNTH_IF(p, P, v, 12);                                                                   \
// SYNTH_IF(p, P, v, 13);                                                                   \
// SYNTH_IF(p, P, v, 14);                                                                   \
// SYNTH_IF(p, P, v, 15);                                                                   \
// SYNTH_IF(p, P, v, 16);
*/

static void synth_print_hook(HeavyContextInterface* c, const char* name, const char* str,
                             const HvMessage* m) {
  (void)c;
  double millis = 1000.0 * ((double)hv_msg_getTimestamp(m)) / SAMPLE_RATE;
  print_debug_3("[@ %.3fms] %s: %s\n", millis, name, str);
}

int synth_init(mostro* m) {
  synth_t* x = &m->synth;
  x->heavy = hv_synth_new(SAMPLE_RATE);
  hv_setPrintHook(x->heavy, synth_print_hook);

// clang-format off
  #ifdef DEBUG
    assert(CHANNELS_SW == 1);
    int output_channels = hv_getNumOutputChannels(x->heavy);
    assert(output_channels == CHANNELS_SW);
  #endif
  // clang-format on

  return 0;
}

void synth_destroy(mostro* m) {
  hv_delete(m->synth.heavy);
}

int synth_process(mostro* m, float* buffer, uint n) {
  return hv_processInline(m->synth.heavy, null, buffer, n);
}

void synth_set_pitch(mostro* m, int voice, float pitch) {
  SYNTH_P_V(param, HV_SYNTH_PARAM_IN_PITCH, voice);
  hv_sendFloatToReceiver(m->synth.heavy, param, pitch);
}

void synth_set_velocity(mostro* m, int voice, int velocity) {
  SYNTH_P_V(param, HV_SYNTH_PARAM_IN_VELOCITY, voice);
  hv_sendFloatToReceiver(m->synth.heavy, param, velocity);
}

void synth_set_index(mostro* m, int voice, float f) {
  SYNTH_P_V(param, HV_SYNTH_PARAM_IN_INDEX, voice);
  hv_sendFloatToReceiver(m->synth.heavy, param, f);
}

void synth_set_ratio(mostro* m, int voice, float f) {
  SYNTH_P_V(param, HV_SYNTH_PARAM_IN_RATIO, voice);
  hv_sendFloatToReceiver(m->synth.heavy, param, f);
}

void synth_set_feedback(mostro* m, int voice, float f) {
  SYNTH_P_V(param, HV_SYNTH_PARAM_IN_FEEDBACK, voice);
  hv_sendFloatToReceiver(m->synth.heavy, param, f);
}

void synth_set_env_amt(mostro* m, int voice, float f) {
  SYNTH_P_V(param, HV_SYNTH_PARAM_IN_ENVELOPE_AMMOUNT, voice);
  hv_sendFloatToReceiver(m->synth.heavy, param, f);
}

void synth_set_attack(mostro* m, int voice, float f) {
  SYNTH_P_V(param, HV_SYNTH_PARAM_IN_ATTACK, voice);
  hv_sendFloatToReceiver(m->synth.heavy, param, f);
}

void synth_set_release(mostro* m, int voice, float f) {
  SYNTH_P_V(param, HV_SYNTH_PARAM_IN_RELEASE, voice);
  hv_sendFloatToReceiver(m->synth.heavy, param, f);
}

void synth_set_lfo_amt(mostro* m, int voice, float f) {
  SYNTH_P_V(param, HV_SYNTH_PARAM_IN_LFO_AMMOUNT, voice);
  hv_sendFloatToReceiver(m->synth.heavy, param, f);
}

void synth_lfo_reset(mostro* m, int voice) {
  SYNTH_P_V(param, HV_SYNTH_PARAM_IN_LFO_RESET, voice);
  hv_sendFloatToReceiver(m->synth.heavy, param, 1.0);
}

void synth_set_lfo_freq(mostro* m, int voice, float f) {
  SYNTH_P_V(param, HV_SYNTH_PARAM_IN_LFO_FREQ, voice);
  hv_sendFloatToReceiver(m->synth.heavy, param, f);
}

void synth_set_lfo_dest_index(mostro* m, int voice, bool value) {
  SYNTH_P_V(param, HV_SYNTH_PARAM_IN_LFO_DEST_INDEX, voice);
  hv_sendFloatToReceiver(m->synth.heavy, param, value);
}

void synth_set_lfo_dest_ratio(mostro* m, int voice, bool value) {
  SYNTH_P_V(param, HV_SYNTH_PARAM_IN_LFO_DEST_RATIO, voice);
  hv_sendFloatToReceiver(m->synth.heavy, param, value);
}

void synth_set_lfo_dest_feedback(mostro* m, int voice, bool value) {
  SYNTH_P_V(param, HV_SYNTH_PARAM_IN_LFO_DEST_FEEDBACK, voice);
  hv_sendFloatToReceiver(m->synth.heavy, param, value);
}

void synth_set_lfo_dest_pitch(mostro* m, int voice, bool value) {
  SYNTH_P_V(param, HV_SYNTH_PARAM_IN_LFO_DEST_PITCH, voice);
  hv_sendFloatToReceiver(m->synth.heavy, param, value);
}

void synth_set_glide(mostro* m, int voice, float f) {
  SYNTH_P_V(param, HV_SYNTH_PARAM_IN_GLIDE, voice);
  hv_sendFloatToReceiver(m->synth.heavy, param, f);
}

void synth_set_delay_ammount(mostro* m, int voice, float f) {
  (void)voice;
  hv_sendFloatToReceiver(m->synth.heavy, HV_SYNTH_PARAM_IN_DELAY_AMMOUNT, f);
}

void synth_set_delay_feedback(mostro* m, int voice, float f) {
  (void)voice;
  hv_sendFloatToReceiver(m->synth.heavy, HV_SYNTH_PARAM_IN_DELAY_FEEDBACK, f);
}

void synth_set_delay_time(mostro* m, int voice, float f) {
  (void)voice;
  hv_sendFloatToReceiver(m->synth.heavy, HV_SYNTH_PARAM_IN_DELAY_TIME, f);
}

void synth_set_volume(mostro* m, int voice, float f) {
  // TODO layers volume...
  (void)voice;
  hv_sendFloatToReceiver(m->synth.heavy, HV_SYNTH_PARAM_IN_VOLUME, f);
}

void synth_set_sub(mostro* m, int voice, bool value) {
  SYNTH_P_V(param, HV_SYNTH_PARAM_IN_SUB, voice);
  hv_sendFloatToReceiver(m->synth.heavy, param, value);
}

void synth_set_env_dest_index(mostro* m, int voice, bool value) {
  SYNTH_P_V(param, HV_SYNTH_PARAM_IN_ENVELOPE_DEST_INDEX, voice);
  hv_sendFloatToReceiver(m->synth.heavy, param, value);
}

void synth_set_env_dest_ratio(mostro* m, int voice, bool value) {
  SYNTH_P_V(param, HV_SYNTH_PARAM_IN_ENVELOPE_DEST_RATIO, voice);
  hv_sendFloatToReceiver(m->synth.heavy, param, value);
}

void synth_set_env_dest_feedback(mostro* m, int voice, bool value) {
  SYNTH_P_V(param, HV_SYNTH_PARAM_IN_ENVELOPE_DEST_FEEDBACK, voice);
  hv_sendFloatToReceiver(m->synth.heavy, param, value);
}

void synth_set_amp(mostro* m, int voice, bool value) {
  SYNTH_P_V(param, HV_SYNTH_PARAM_IN_ENVELOPE_DEST_AMP, voice);
  hv_sendFloatToReceiver(m->synth.heavy, param, value);
}

void synth_set_sustain(mostro* m, int voice, bool value) {
  SYNTH_P_V(param, HV_SYNTH_PARAM_IN_SUSTAIN, voice);
  hv_sendFloatToReceiver(m->synth.heavy, param, value);
}

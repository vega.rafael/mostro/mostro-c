#ifndef AUDIO_IO_H
#define AUDIO_IO_H

// https://stackoverflow.com/questions/32672333/including-alsa-asoundlib-h-and-sys-time-h-results-in-multiple-definition-co
#define _STRUCT_TIMESPEC 1
#include <alsa/asoundlib.h>

#include <stdbool.h>
#include <stdint.h>

#include "basics.h"

#define BLOCK_SIZE 128
#define SAMPLE_RATE 48000
#define CHANNELS_HW 2
#define CHANNELS_SW 1

struct audio_io_t {
  bool restarting;
  snd_pcm_t* playback_handle;
  int32_t silence[BLOCK_SIZE * CHANNELS_HW];

  // Buffer for storing processed samples
  float process_buffer[BLOCK_SIZE * CHANNELS_SW];

  // Buffer for storing output buffer
  int32_t output_buffer[BLOCK_SIZE * CHANNELS_HW];
};
typedef struct audio_io_t audio_io_t;

int audio_io_start(audio_io_t* x, char* device);
int audio_io_output(audio_io_t* x, int n);
void audio_io_stop(audio_io_t* x);

#endif /* AUDIO_IO_H */

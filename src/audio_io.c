#include <assert.h>
#include <stdbool.h>
#include <stdio.h>

#include "audio_io.h"
#include "mostro.h"

static int audio_io_config(snd_pcm_t* device) {
  snd_pcm_hw_params_t* hw_params;
  int err;
  uint periods = 2; // sets a buffer size twice as big as the block size.
  // uint bit_depth = 32;

  /* allocate memory for hardware parameter structure */
  if ((err = snd_pcm_hw_params_malloc(&hw_params)) < 0) {
    print_debug("cannot allocate parameter structure (%s)\n", snd_strerror(err));
    return 1;
  }

  /* fill structure from current audio parameters */
  if ((err = snd_pcm_hw_params_any(device, hw_params)) < 0) {
    print_debug("cannot initialize parameter structure (%s)\n", snd_strerror(err));
    return 1;
  }

  /* set access type, sample rate, sample format, channels */
  // TODO NON INTERLEAVED?
  if ((err = snd_pcm_hw_params_set_access(device, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED)) < 0) {
    print_debug("cannot set access type: %s\n", snd_strerror(err));
    return 1;
  }
  /* if ((err = snd_pcm_hw_params_set_format(device, hw_params, SND_PCM_FORMAT_S32_LE)) < 0) { */
  if ((err = snd_pcm_hw_params_set_format(device, hw_params, SND_PCM_FORMAT_S32_LE)) < 0) {
    print_debug("cannot set sample format: %s\n", snd_strerror(err));
    return 1;
  }
  uint requested_sample_rate = SAMPLE_RATE;
  if ((err = snd_pcm_hw_params_set_rate_near(device, hw_params, &requested_sample_rate, 0)) < 0) {
    print_debug("cannot set sample rate: %s\n", snd_strerror(err));
    return 1;
  }
  if (requested_sample_rate != SAMPLE_RATE) {
    print_debug_2("Could not set requested sample rate, asked for %d got %d\n", requested_sample_rate,
                SAMPLE_RATE);
    return 1;
  }
  if ((err = snd_pcm_hw_params_set_channels(device, hw_params, CHANNELS_HW)) < 0) {
    print_debug("cannot set channel count: %s\n", snd_strerror(err));
    return 1;
  }

  uint requested_periods = periods;
  if ((err = snd_pcm_hw_params_set_periods_near(device, hw_params, &periods, 0)) < 0) {
    print_debug_2("Error setting # periods to %d: %s\n", periods, snd_strerror(err));
    return 1;
  }
  if (requested_periods != periods) {
    print_debug_2("Could not set requested periods, asked for %d got %d\n", requested_periods,
                periods);
    return 1;
  }

  snd_pcm_uframes_t buffer_size = BLOCK_SIZE * periods;
  snd_pcm_uframes_t requested_buffer_size = buffer_size;
  if ((err = snd_pcm_hw_params_set_buffer_size_near(device, hw_params, &buffer_size)) < 0) {
    print_debug_2("Error setting buffer_size %lu frames: %s\n", buffer_size, snd_strerror(err));
    return 1;
  }
  if (buffer_size != requested_buffer_size) {
    print_debug_2("Could not set requested buffer size, asked for %lu got %lu\n",
                requested_buffer_size, buffer_size);
  }

  if ((err = snd_pcm_hw_params(device, hw_params)) < 0) {
    print_debug("Error setting HW params: %s\n", snd_strerror(err));
    return 1;
  }

  snd_pcm_hw_params_free(hw_params);

  return 0;
}

int audio_io_start(audio_io_t* x, char* snd_device_out) {
  int err = 0;

  for (uint i = 0; i < BLOCK_SIZE * CHANNELS_HW; ++i) {
    x->silence[i] = 0;
  }

  x->restarting = true;

  if ((err = snd_pcm_open(&x->playback_handle, snd_device_out, SND_PCM_STREAM_PLAYBACK, 0)) < 0) {
    print_debug_2("cannot open output audio device %s: %s\n", snd_device_out, snd_strerror(err));
    return -1;
  }

  err = audio_io_config(x->playback_handle);

  return err;
}

int audio_io_output(audio_io_t* x, int n) {
  // Convert processed samples (float) to ints
  // TODO: Use NEON here or modify driver to accept non interleaved.
  int32_t out_sample = 0;
  for (int i = 0; i < n; ++i) {
    out_sample = x->process_buffer[i] * (float)INT32_MAX;
    // TODO: Which sould be silent? L or R?
    x->output_buffer[2 * i] = out_sample;
    x->output_buffer[2 * i + 1] = 0;
  }

  if (x->restarting) {
    x->restarting = false;
    snd_pcm_drop(x->playback_handle);
    snd_pcm_prepare(x->playback_handle);
    snd_pcm_writei(x->playback_handle, x->silence, n);
  }

  int n_output = 0;
  while ((n_output = snd_pcm_writei(x->playback_handle, x->output_buffer, n)) < 0) {
    if (n_output == -EAGAIN) {
      print_debug("%s", "EAGAINd\n");
      continue;
    }
    x->restarting = true;
    snd_pcm_prepare(x->playback_handle);

    print_debug("%s", "Output buffer underrun\n");
  }

  if (n_output != n) {
    print_debug("%s", "Short write to output buffer\n");
  }

  return 0;
}

void audio_io_stop(audio_io_t* x) {
  snd_pcm_close(x->playback_handle);
}

#pragma GCC system_header
/** Copyright (c) Outer Space Sounds 2020 */

#ifndef _HEAVY_SYNTH_H_
#define _HEAVY_SYNTH_H_

#include "HvHeavy.h"

#ifdef __cplusplus
extern "C" {
#endif

#if HV_APPLE
#pragma mark - Heavy Context
#endif

typedef enum {
  HV_SYNTH_PARAM_IN_ATTACK_0 = 0x749B58EE, // attack_0
  HV_SYNTH_PARAM_IN_DELAY_AMMOUNT = 0xC856C6A4, // delay_ammount
  HV_SYNTH_PARAM_IN_DELAY_FEEDBACK = 0x3BF5CB83, // delay_feedback
  HV_SYNTH_PARAM_IN_DELAY_TIME = 0x6BB437E, // delay_time
  HV_SYNTH_PARAM_IN_ENVELOPE_AMMOUNT_0 = 0xBA00F8C0, // envelope_ammount_0
  HV_SYNTH_PARAM_IN_ENVELOPE_DEST_AMP_0 = 0x41FF1BBA, // envelope_dest_amp_0
  HV_SYNTH_PARAM_IN_ENVELOPE_DEST_FEEDBACK_0 = 0x8B6FA2BC, // envelope_dest_feedback_0
  HV_SYNTH_PARAM_IN_ENVELOPE_DEST_INDEX_0 = 0xC9C99193, // envelope_dest_index_0
  HV_SYNTH_PARAM_IN_ENVELOPE_DEST_RATIO_0 = 0xD9A0B41F, // envelope_dest_ratio_0
  HV_SYNTH_PARAM_IN_FEEDBACK_0 = 0xB5359052, // feedback_0
  HV_SYNTH_PARAM_IN_GLIDE_0 = 0x26CA1C30, // glide_0
  HV_SYNTH_PARAM_IN_INDEX_0 = 0xDFE0E90B, // index_0
  HV_SYNTH_PARAM_IN_LFO_AMMOUNT_0 = 0xE400D975, // lfo_ammount_0
  HV_SYNTH_PARAM_IN_LFO_DEST_FEEDBACK_0 = 0x87782412, // lfo_dest_feedback_0
  HV_SYNTH_PARAM_IN_LFO_DEST_INDEX_0 = 0xA568939A, // lfo_dest_index_0
  HV_SYNTH_PARAM_IN_LFO_DEST_PITCH_0 = 0x810B4C85, // lfo_dest_pitch_0
  HV_SYNTH_PARAM_IN_LFO_DEST_RATIO_0 = 0xC8C38FC5, // lfo_dest_ratio_0
  HV_SYNTH_PARAM_IN_LFO_FREQ_0 = 0xEC11BCEA, // lfo_freq_0
  HV_SYNTH_PARAM_IN_LFO_FREQ_HZ_0 = 0x87A5BA85, // lfo_freq_hz_0
  HV_SYNTH_PARAM_IN_LFO_RESET_0 = 0x61FE98A0, // lfo_reset_0
  HV_SYNTH_PARAM_IN_PITCH_0 = 0x3A84DFE8, // pitch_0
  HV_SYNTH_PARAM_IN_RATIO_0 = 0xAACFB70A, // ratio_0
  HV_SYNTH_PARAM_IN_RELEASE_0 = 0xF0598C7A, // release_0
  HV_SYNTH_PARAM_IN_SUB_0 = 0x5FC9B0A5, // sub_0
  HV_SYNTH_PARAM_IN_SUSTAIN_0 = 0x26925CD4, // sustain_0
  HV_SYNTH_PARAM_IN_VELOCITY_0 = 0x619F16FF, // velocity_0
  HV_SYNTH_PARAM_IN_VOLUME = 0xB1642755, // volume
} Hv_synth_ParameterIn;


/**
 * Creates a new patch instance.
 * Sample rate should be positive and in Hertz, e.g. 44100.0.
 */
HeavyContextInterface *hv_synth_new(double sampleRate);

/**
 * Creates a new patch instance.
 * @param sampleRate  Sample rate should be positive (> 0) and in Hertz, e.g. 48000.0.
 * @param poolKb  Pool size is in kilobytes, and determines the maximum amount of memory
 *   allocated to messages at any time. By default this is 10 KB.
 * @param inQueueKb  The size of the input message queue in kilobytes. It determines the
 *   amount of memory dedicated to holding scheduled messages between calls to
 *   process(). Default is 2 KB.
 * @param outQueueKb  The size of the output message queue in kilobytes. It determines the
 *   amount of memory dedicated to holding scheduled messages to the default sendHook.
 *   See getNextSentMessage() for info on accessing these messages. Default is 0 KB.
 */
HeavyContextInterface *hv_synth_new_with_options(double sampleRate, int poolKb, int inQueueKb, int outQueueKb);

#ifdef __cplusplus
} // extern "C"
#endif

#endif // _HEAVY_SYNTH_H_

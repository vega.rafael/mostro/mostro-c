#include <assert.h>
#include <signal.h>
#include <stdbool.h>
#include <stdint.h>

#include "mostro.h"

bool should_quit = false;

// Handle terminate or ctrl-c signal from OS.
void quit(int sig) {
  signal(sig, SIG_IGN);
  should_quit = true;
}

int main(int argc, char** argv) {
  (void)argc;
  (void)argv;

  sv_timespec start_time = {0};
  sv_timespec_get(&start_time);

  signal(SIGINT, quit);
  signal(SIGTERM, quit);

  mostro m;
  mostro_init(&m);

  audio_io_t audio_io;
  if (audio_io_start(&audio_io, "hw:0,0")) {
    print_debug("%s", "Failed to initialize audio io\n");
    goto cleanup;
    return -1;
  }

  while (!should_quit) {
    // It's okay to have non-audio granularity in the time. It's only used for control.
    sv_timespec now = {0};
    sv_timespec_get(&now);
    double t = sv_timespec_diff(&now, &start_time);
    m.time = t;

    buttons_process(&m);

    // TODO: try smaller block sizes and poll for midi and button events in
    //       between blocks.
    synth_process(&m, audio_io.process_buffer, BLOCK_SIZE);
    if (audio_io_output(&audio_io, BLOCK_SIZE)) {
      print_debug("%s", "Error writing to audio io\n");
    }
  }

  audio_io_stop(&audio_io);

cleanup:
  buttons_destroy(&m);
  synth_destroy(&m);
  return 0;
}

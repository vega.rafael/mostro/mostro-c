#include <beaglebone_pruio.h>

#include "buttons_knobs.h"
#include "mostro.h"
#include "scales.h"


// TODO:
// * Change api of pru library, it needs global state. Should pass void* pointers to callbacks
//   instead.
// * MIDI Messages are not handled correctly in PRU right now. Process all in ARM? Or implement
//   complete midi parser in PRU?

#define KEY_00 P9_41A
#define KEY_01 P9_16
#define KEY_02 P9_27
#define KEY_03 P8_46
#define KEY_04 P8_44
#define KEY_05 P8_43
#define KEY_06 P8_42
#define KEY_07 P8_41
#define KEY_08 P8_40
#define KEY_09 P8_39
#define KEY_10 P8_37
#define KEY_11 P8_36
#define KEY_12 P8_35

#define KNOB_INDEX 3
#define KNOB_RATIO 5
#define KNOB_FEEDBACK 4
#define KNOB_ENV_AMT 6
#define KNOB_ATTACK 7
#define KNOB_RELEASE 8
#define KNOB_LFO_AMT 11
#define KNOB_LFO_FREQ 13
#define KNOB_GLIDE 10
#define KNOB_DELAY 12
#define KNOB_VOLUME 9

#define BTN_PRESET_PLUS P8_07
#define BTN_PRESET_MINUS P8_08
#define BTN_SUB P8_10
#define BTN_ENV_DEST P8_09
#define BTN_AMP P8_11
#define BTN_SUSTAIN P8_30
#define BTN_LOOP P9_42A
#define BTN_LFO_DEST P8_45
#define BTN_TAP P8_38
#define BTN_LAYER P8_32
#define BTN_SHIFT P8_31
#define BTN_OCT_MINUS P8_34
#define BTN_OCT_PLUS P8_33

static void buttons_handle_key(mostro* m, int key_number, int value);
static void buttons_init_tap(mostro* m);
static void buttons_handle_tap(mostro* m, float f);
static void buttons_tap_check_timeout(mostro* m);

int buttons_init(mostro* m) {
  assert(m);
  buttons_t* x = &m->buttons;
  x->first_time = true;

  if (beaglebone_pruio_start()) {
    print_debug("%s", "Could not load PRU programs\n");
    return 1;
  }

  int big_keys[] = {KEY_00, KEY_01, KEY_02, KEY_03, KEY_04, KEY_05, KEY_06,
                    KEY_07, KEY_08, KEY_09, KEY_10, KEY_11, KEY_12};
  uint n = sizeof(big_keys) / sizeof(int);
  for (uint i = 0; i < n; ++i) {
    if (beaglebone_pruio_init_gpio_pin(big_keys[i], BEAGLEBONE_PRUIO_GPIO_MODE_INPUT)) {
      print_debug("Could not initialize key %i\n", i);
      return 1;
    }
  }

  int bits = 8;
  int window_size = 256;
  int knobs[] = {KNOB_INDEX,  KNOB_RATIO,   KNOB_FEEDBACK, KNOB_ENV_AMT,
                 KNOB_ATTACK, KNOB_RELEASE, KNOB_LFO_AMT,  KNOB_LFO_FREQ,
                 KNOB_GLIDE,  KNOB_DELAY,   KNOB_VOLUME};
  n = sizeof(knobs) / sizeof(int);
  for (uint i = 0; i < n; ++i) {
    if (beaglebone_pruio_init_adc_pin(knobs[i], bits, window_size)) {
      print_debug("Could not initialize knob %i\n", i);
      return 1;
    }
  }

  int buttons[] = {BTN_PRESET_PLUS, BTN_PRESET_MINUS, BTN_SUB,      BTN_ENV_DEST, BTN_AMP,
                   BTN_SUSTAIN,     BTN_LOOP,         BTN_LFO_DEST, BTN_TAP,      BTN_LAYER,
                   BTN_SHIFT,       BTN_OCT_MINUS,    BTN_OCT_PLUS};
  n = sizeof(buttons) / sizeof(int);
  for (uint i = 0; i < n; ++i) {
    if (beaglebone_pruio_init_gpio_pin(buttons[i], BEAGLEBONE_PRUIO_GPIO_MODE_INPUT)) {
      print_debug("Could not initialize button %i\n", i);
      return 1;
    }
  }

  buttons_init_tap(m);

  return 0;
}

int buttons_destroy(mostro* m) {
  (void)m;
  return beaglebone_pruio_stop();
}

int buttons_process(mostro* m) {
  beaglebone_pruio_message message;

  while (beaglebone_pruio_messages_are_available()) {
    beaglebone_pruio_read_message(&message);

    if (message.is_gpio) {
      switch (message.gpio_number) {
        case KEY_00:
          buttons_handle_key(m, 0, message.value);
          break;
        case KEY_01:
          buttons_handle_key(m, 1, message.value);
          break;
        case KEY_02:
          buttons_handle_key(m, 2, message.value);
          break;
        case KEY_03:
          buttons_handle_key(m, 3, message.value);
          break;
        case KEY_04:
          buttons_handle_key(m, 4, message.value);
          break;
        case KEY_05:
          buttons_handle_key(m, 5, message.value);
          break;
        case KEY_06:
          buttons_handle_key(m, 6, message.value);
          break;
        case KEY_07:
          buttons_handle_key(m, 7, message.value);
          break;
        case KEY_08:
          buttons_handle_key(m, 8, message.value);
          break;
        case KEY_09:
          buttons_handle_key(m, 9, message.value);
          break;
        case KEY_10:
          buttons_handle_key(m, 10, message.value);
          break;
        case KEY_11:
          buttons_handle_key(m, 11, message.value);
          break;
        case KEY_12:
          // This first time thing is for MOSTROv3 which does not have a 13th key.
          if (m->buttons.first_time) {
            m->buttons.first_time = false;
            // buttons_handle_key(m, 12, 0);
          }
          else {
            buttons_handle_key(m, 12, message.value);
          }
          break;

        case BTN_PRESET_PLUS:
          // TODO
          break;

        case BTN_PRESET_MINUS:
          // TODO
          break;

        case BTN_SUB:
          if (message.value == 1) {
            mostro_set_btn_sub(m);
          }
          break;

        case BTN_ENV_DEST:
          if (message.value == 1) {
            mostro_set_btn_env_dest(m);
          }
          break;

        case BTN_AMP:
          if (message.value == 1) {
            mostro_set_btn_amp(m);
          }
          break;

        case BTN_SUSTAIN:
          if (message.value == 1) {
            mostro_set_btn_sustain(m);
          }
          break;

        case BTN_LOOP:
          // TODO
          break;

        case BTN_LFO_DEST:
          if (message.value == 1) {
            mostro_set_btn_lfo_dest(m);
          }
          break;

        case BTN_TAP:
          buttons_handle_tap(m, message.value);
          break;

        case BTN_LAYER:
          break;

        case BTN_SHIFT:
          break;

        case BTN_OCT_MINUS:
          if (message.value == 1) {
            mostro_set_btn_octave_minus(m);
          }
          break;

        case BTN_OCT_PLUS:
          if (message.value == 1) {
            mostro_set_btn_octave_plus(m);
          }
          break;

        default:
          assert(false || "Unknown button?");
          break;
      }
    }
    else { // message is adc
      switch (message.adc_channel) {
        case KNOB_INDEX:
          mostro_set_pot_index(m, message.value);
          break;

        case KNOB_RATIO:
          mostro_set_pot_ratio(m, message.value);
          break;

        case KNOB_FEEDBACK:
          mostro_set_pot_feedback(m, message.value);
          break;

        case KNOB_ENV_AMT:
          mostro_set_pot_env_amt(m, message.value);
          break;

        case KNOB_ATTACK:
          mostro_set_pot_attack(m, message.value);
          break;

        case KNOB_RELEASE:
          mostro_set_pot_release(m, message.value);
          break;

        case KNOB_LFO_AMT:
          mostro_set_pot_lfo_amt(m, message.value);
          break;

        case KNOB_LFO_FREQ:
          mostro_set_pot_lfo_freq(m, message.value);
          break;

        case KNOB_GLIDE:
          mostro_set_pot_glide(m, message.value);
          break;

        case KNOB_DELAY:
          if (m->buttons.tap_is_down) {
            mostro_set_pot_delay_feedback(m, message.value);
          }
          else {
            mostro_set_pot_delay_ammount(m, message.value);
          }
          break;

        case KNOB_VOLUME:
          mostro_set_pot_volume(m, message.value);
          break;

        default:
          assert(false || "Unknown knob?");
          break;
      }
      /* printf("ADC Channel :%u, Value: %f, Raw Value: %i\n", message.adc_channel, message.value,
       * message.raw_value); */
      /* if(message.adc_channel==5 || message.adc_channel==3) { */
      // printf("C:%2u, V:%0.4f\n", message.adc_channel, message.value);
      /* } */
    }
  }

  buttons_tap_check_timeout(m);

  return 0;
}

static void buttons_handle_key(mostro* m, int key_number, int value) {
  if (value == 1) {
    mostro_start_note_key(m, key_number);
  }
  else {
    mostro_end_note_key(m, key_number);
  }
}

///////////////////////////////////////////////////////////////////////////////
// TAP
//

// Tap tempo button can be held down the knob will change the delay feedback
// instead of the ammount.
// Also, when "clicking" 4 times, sets the delay time

static void buttons_init_tap(mostro* m) {
  buttons_t* x = &m->buttons;
  x->n_tap_times = 0;
  for (uint i = 0; i < BUTTONS_MAX_TIMES; ++i) {
    x->tap_times[i] = 0.0;
  }
  x->tap_is_down = false;
  x->tap_is_tapping = false;
}

static void buttons_tap_check_timeout(mostro* m) {
  // Tap tempo timeout
  buttons_t* x = &m->buttons;
  if (!x->tap_is_down && x->tap_is_tapping) {

    uint i = x->n_tap_times;
    if (i == 0) {
      i = BUTTONS_MAX_TIMES - 1;
    }
    else {
      i--;
    }
    assert(i < BUTTONS_MAX_TIMES);
    double now = m->time;
    double previous_time = x->tap_times[i];
    if (now - previous_time >= BUTTONS_TAP_TIMEOUT) {
      x->tap_is_tapping = false;
      x->n_tap_times = 0;
    }
  }
}

static void buttons_handle_tap(mostro* m, float f) {
  buttons_t* x = &m->buttons;
  if (f == 1) {
    x->tap_is_down = true;
    x->tap_is_tapping = true;

    x->tap_times[x->n_tap_times] = m->time;
    x->n_tap_times++;
    if (x->n_tap_times >= BUTTONS_MAX_TIMES) {
      x->n_tap_times = 0;
    }

    if (x->n_tap_times == 0) {
      float average = 0.0;
      float duration = 0.0;
      for (uint i = 1; i < BUTTONS_MAX_TIMES; ++i) {
        duration = x->tap_times[i] - x->tap_times[i - 1];
        average += duration;
      }
      float millis = average / (BUTTONS_MAX_TIMES - 1) * 1000.0;
      mostro_set_delay_time(m, millis);
      // print_debug("tap time %f\n", millis);
    }
  }
  else {
    x->tap_is_down = false;
  }
}

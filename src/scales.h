#ifndef SCALES_H
#define SCALES_H

#include "basics.h"

typedef struct mostro mostro;

#define MAX_TUNINGS 99
#define MAX_NOTES 128

struct scales_t {
  float tunings[MAX_TUNINGS][MAX_NOTES];
  uint num_tunings;
};
typedef struct scales_t scales_t;

void scales_init(mostro* m);
float scales_note_to_freq(mostro* m, uint note_number);

#endif /* SCALES */

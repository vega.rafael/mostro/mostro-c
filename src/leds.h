#ifndef LEDS_H
#define LEDS_H

#include <stdbool.h>
#include <beaglebone_pruio_pins.h>

#define LED_SUB P9_12
#define LED_AMP P9_14
#define LED_SUST P9_15
#define LED_LOOP P9_26
#define LED_ENV_INDEX P8_12
#define LED_ENV_RATIO P8_15
#define LED_ENV_FEEDBACK P8_16
#define LED_LFO_INDEX P9_21
#define LED_LFO_RATIO P9_22
#define LED_LFO_FEEDBACK P9_23
#define LED_LFO_PITCH P9_24

typedef struct mostro mostro;

struct leds_t {
  bool a;
};
typedef struct leds_t leds_t;

int leds_init(mostro* m);
void led_set(int led, int on);
void led_on(int led);
void led_off(int led);


#endif /* LEDS */

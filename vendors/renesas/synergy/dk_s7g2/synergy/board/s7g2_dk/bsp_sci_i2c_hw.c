/***********************************************************************************************************************
 * Copyright [2019] Renesas Electronics Corporation and/or its licensors. All Rights Reserved.
 * 
 * This file is part of Renesas SynergyTM Software Package (SSP)
 *
 * The contents of this file (the "contents") are proprietary and confidential to Renesas Electronics Corporation
 * and/or its licensors ("Renesas") and subject to statutory and contractual protections.
 *
 * This file is subject to a Renesas SSP license agreement. Unless otherwise agreed in an SSP license agreement with
 * Renesas: 1) you may not use, copy, modify, distribute, display, or perform the contents; 2) you may not use any name
 * or mark of Renesas for advertising or publicity purposes or in connection with your use of the contents; 3) RENESAS
 * MAKES NO WARRANTY OR REPRESENTATIONS ABOUT THE SUITABILITY OF THE CONTENTS FOR ANY PURPOSE; THE CONTENTS ARE PROVIDED
 * "AS IS" WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT; AND 4) RENESAS SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, OR
 * CONSEQUENTIAL DAMAGES, INCLUDING DAMAGES RESULTING FROM LOSS OF USE, DATA, OR PROJECTS, WHETHER IN AN ACTION OF
 * CONTRACT OR TORT, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THE CONTENTS. Third-party contents
 * included in this file may be subject to different terms.
 **********************************************************************************************************************/
/***********************************************************************************************************************
* File Name    : bsp_sci_i2c_hw.c
* Description  : This module calls any initialization code specific to this BSP.
***********************************************************************************************************************/

/*******************************************************************************************************************//**
 * @addtogroup BSP_BOARD_DK2M_INIT
 *
 * @{
 **********************************************************************************************************************/

/***********************************************************************************************************************
Includes   <System Includes> , "Project Includes"
***********************************************************************************************************************/
#include "bsp_api.h"
#include "bsp_sci_i2c.h"

#if defined(BSP_BOARD_S7G2_DK)

/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/

/***********************************************************************************************************************
Typedef definitions
***********************************************************************************************************************/

/***********************************************************************************************************************
Exported global variables (to be accessed by other files)
***********************************************************************************************************************/
 
/***********************************************************************************************************************
Private global variables and functions
***********************************************************************************************************************/

/*******************************************************************************************************************//**
 * @brief      Performs any initialization specific to this BSP.
 *
 * @param[in]  p_args         Pointer to arguments of the user's choice.
 **********************************************************************************************************************/
/* AE, AF */
#define I2C_WR_ADDR  0xA0
#define I2C_RD_ADDR  0xA1


/*******************************************************************************
* Function name : bsp_sciiic_tx
* Description   : SCI TX in simple IIC mode
* Arguments     : Data to write and number of bytes of Data.

* Return value  : none
*******************************************************************************/
void bsp_sciiic_tx (uint8_t *w_data, uint32_t byte_number)
{
    /* Generate START condition */
    bsp_sciiic_start();                   /* Generate START */
    bsp_sciiic_wait_IICSTIF_serial_out(); /* Wait Flag and Set serial data out */

    bsp_sciiic_write(I2C_WR_ADDR);         /* Write Slave address and write-bit */

    /* Write DATA */
    for (uint32_t i=0; i<byte_number; i++)
    {
        /* Write Data */
        bsp_sciiic_write(w_data[i]);
        bsp_sciiic_wait_TEND();
        if (1 == bsp_sciiic_chk_IICACKR())
        {
            break;
        }
    }

    /* Generate STOP condition */
    bsp_sciiic_stop();
    bsp_sciiic_wait_IICSTIF_highz();

}

/*******************************************************************************
* Function name : bsp_sciiic_rx
* Description   : SCI RX in simple IIC mode
* Arguments     : Address, Read Buffer, length of bytes to read

* Return value  : none
*******************************************************************************/
void bsp_sciiic_rx (uint8_t *w_addr, uint8_t *r_data, uint32_t byte_number)
{
    /* Generate START condition */
    bsp_sciiic_start();
    bsp_sciiic_wait_IICSTIF_serial_out();

    /* Write Slave address and write-bit */
    bsp_sciiic_write(I2C_WR_ADDR);
    bsp_sciiic_wait_TEND();

    /* Write ADDR0 */
    bsp_sciiic_write(w_addr[0]);
    bsp_sciiic_wait_TEND();

    /* Write ADDR1 */
    bsp_sciiic_write(w_addr[1]);
    bsp_sciiic_wait_TEND();

    /* Generate START condition */
    bsp_sciiic_restart();
    bsp_sciiic_wait_IICSTIF_serial_out();

    /* Write Slave address and read-bit */
    bsp_sciiic_write(I2C_RD_ADDR);
    bsp_sciiic_wait_TEND();

    R_SCI7->SIMR2_b.IICACKT = 0;

    /* Read DATA */
    for (uint32_t i=0; i<=(byte_number); i++)
    {
        if (byte_number == i)
        {
            R_SCI7->SIMR2_b.IICACKT = 1;
        }
        /* Dummy write */
        bsp_sciiic_write(0xFF);
        bsp_sciiic_wait_TEND();

        /* Read DATA */
        bsp_sciiic_wait_RDRF();
        r_data[i] = R_SCI7->RDR;
        bsp_sciiic_wait_TEND();
    }

    /* Generate STOP condition */
    bsp_sciiic_stop();
    bsp_sciiic_wait_IICSTIF_highz();

}

/************************************************************************************************************************************
*                             Low Level Functions                                                                                   *
*                                                                                                                                   *
*************************************************************************************************************************************/

/*******************************************************************************
* Function name : bsp_init_sciiic
* Description   : Initialize SCI in simple IIC mode
* Arguments     : none

* Return value  : none
*******************************************************************************/
void bsp_init_sciiic(void)
{
    /* Initialize SCI's I2C simple mode */
    /* Clear status and setting */
    R_SCI7->SSR                = 0x00;
    R_SCI7->SCR                = 0x00;
    R_SCI7->SCMR               = 0x00;

    /* Set the FCR.FM bit to 0 */
    R_SCI7->FCR_b.FM           = 0x0;

    R_SCI7->SIMR3_b.IICSDAS    = 0x3;
    R_SCI7->SIMR3_b.IICSCLS    = 0x3;

    /* Set data transmission/reception format in SMR, SCMR, and SEMR */
    /* Async Mode or Simple I2C Mode */
    R_SCI7->SMR_b.CM           = 0x0;
    R_SCI7->SMR_b.CKS          = 0x0;

    /* MSB first; must set to 1 in I2C mode */
    R_SCI7->SCMR_b.SDIR        = 0x1;
    /* no smart card mode, no invert */
    R_SCI7->SCMR_b.SINV        = 0x0;
    R_SCI7->SCMR_b.SMIF        = 0x0;

    R_SCI7->BRR = (uint8_t)((120000000/(64*0.5*100000))-1);
    R_SCI7->MDDR = (uint8_t)((R_SCI7->BRR + 1)*(64*0.5*256*100000)/120000000);

    /* Bit rate modulation, Nose filter disable */
    R_SCI7->SEMR_b.BRME        = 0x1;
    R_SCI7->SEMR_b.NFEN        = 0x0;
    R_SCI7->SNFR_b.NFCS        = 0x1; /* In simple I2C mode, should not set 0. */

    /* I2C mode */
    R_SCI7->SIMR1_b.IICM       = 0x1;
    /* no output delay */
    R_SCI7->SIMR1_b.IICDL      = 0x0;

    /* Internal clock sync */
    R_SCI7->SIMR2_b.IICCSC     = 0x1;

    /* ACK/NACK use */
    R_SCI7->SIMR2_b.IICACKT    = 0x1;

    /* Use reception and transmission Interrupt */
    R_SCI7->SIMR2_b.IICINTM    = 0x1;

    /* Clear all SPMR settings */
    R_SCI7->SPMR               = 0x0;

    /* Enable  TX, Set 2 bits TE, RE at the same time */
    R_SCI7->SCR  = 0x30;
}

/*******************************************************************************
* Function name : bsp_sciiic_start
* Description   : Generate I2C START condition
* Arguments     : none

* Return value  : none
*******************************************************************************/
void bsp_sciiic_start(void)
{
    /* Simultaneously set the SIMR3.IICSTAREQ bit to 1 and the SIMR3l.IICSCLS[1:0] and IICSDAS[1:0] bits to 01b */
    R_SCI7->SIMR3              = 0x51;
}

/*******************************************************************************
* Function name : bsp_sciiic_wait_IICSTIF_serial_out
* Description   : I2C Serial Data Out
* Arguments     : void

* Return value  : none
*******************************************************************************/
void bsp_sciiic_wait_IICSTIF_serial_out(void)
{
    while (0x0 == R_SCI7->SIMR3_b.IICSTIF); /* Wait Star/Restart/Stop condition is completed */
    R_SCI7->SIMR3                   = 0x00; /* Clear IICSTIF, Set serial data out IICSCLS=0 and IICSDAS=0 */
}

/*******************************************************************************
* Function name : bsp_sciiic_restart
* Description   : Generate I2C RESTART condition
* Arguments     : void

* Return value  : none
*******************************************************************************/
void bsp_sciiic_restart(void)
{
    /* Simultaneously set the SIMR3.IICRSTAREQ bit to 1 and the SIMR3l.IICSCLS[1:0] and IICSDAS[1:0] bits to 01b */
    R_SCI7->SIMR3              = 0x52;
}
/*******************************************************************************
* Function name : bsp_sciiic_stop
* Description   : Generate I2C STOP condition
* Arguments     : none

* Return value  : none
*******************************************************************************/
void bsp_sciiic_stop(void)
{
    /* Simultaneously set the SIMR3.IICSTPREQ bit to 1 and the SIMR3l.IICSCLS[1:0] and IICSDAS[1:0] bits to 01b */
    R_SCI7->SIMR3              = 0x54;
}

/*******************************************************************************
* Function name : bsp_sciiic_wait_IICSTIF_highz
* Description   : I2C Wait
* Arguments     : None

* Return value  : none
*******************************************************************************/
void bsp_sciiic_wait_IICSTIF_highz(void)
{
    /* Wait for START/STOP issue */
    while (0x0 == R_SCI7->SIMR3_b.IICSTIF);
    R_SCI7->SIMR3                   = 0xF0; /* Clear IICSTIF=0, Set high impendance IICSCLS=11 and IICSDAS=11 */
}

/*******************************************************************************
* Function name : bsp_sciiic_write
* Description   : Generate I2C START condition
* Arguments     : Address

* Return value  : none
*******************************************************************************/
void bsp_sciiic_write(uint8_t addrata)
{
    /* Write address, data */
    R_SCI7->TDR                         = addrata;
}

/*******************************************************************************
* Function name : bsp_sciiic_wait_TEND
* Description   : Check for Transmit END flag
* Arguments     : none

* Return value  : none
*******************************************************************************/
void bsp_sciiic_wait_TEND(void)
{
    while (!R_SCI7->SSR_b.TEND);
}

/*******************************************************************************
* Function name : bsp_sciiic_chk_IICACKR
* Description   : Check for ACK reception Data Flag
* Arguments     : none

* Return value  : 1 or 0
*******************************************************************************/
uint8_t bsp_sciiic_chk_IICACKR(void)
{
    return (R_SCI7->SISR_b.IICACKR);
}

/*******************************************************************************
* Function name : bsp_sciiic_wait_RDRF
* Description   : Wait for the Receive Data Full Flag
* Arguments     : none

* Return value  : none
*******************************************************************************/
void bsp_sciiic_wait_RDRF(void)
{
    while (!R_SCI7->SSR_b.RDRF);
}

void bsp_sciiic_clear(void)
{
    /* Clear status */
    R_SCI7->SCR                = 0x00;
    R_SCI7->SCR_SMCI           = 0x00;

}
#endif



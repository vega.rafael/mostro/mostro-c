/***********************************************************************************************************************
 * Copyright [2019] Renesas Electronics Corporation and/or its licensors. All Rights Reserved.
 * 
 * This file is part of Renesas SynergyTM Software Package (SSP)
 *
 * The contents of this file (the "contents") are proprietary and confidential to Renesas Electronics Corporation
 * and/or its licensors ("Renesas") and subject to statutory and contractual protections.
 *
 * This file is subject to a Renesas SSP license agreement. Unless otherwise agreed in an SSP license agreement with
 * Renesas: 1) you may not use, copy, modify, distribute, display, or perform the contents; 2) you may not use any name
 * or mark of Renesas for advertising or publicity purposes or in connection with your use of the contents; 3) RENESAS
 * MAKES NO WARRANTY OR REPRESENTATIONS ABOUT THE SUITABILITY OF THE CONTENTS FOR ANY PURPOSE; THE CONTENTS ARE PROVIDED
 * "AS IS" WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT; AND 4) RENESAS SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, OR
 * CONSEQUENTIAL DAMAGES, INCLUDING DAMAGES RESULTING FROM LOSS OF USE, DATA, OR PROJECTS, WHETHER IN AN ACTION OF
 * CONTRACT OR TORT, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THE CONTENTS. Third-party contents
 * included in this file may be subject to different terms.
 **********************************************************************************************************************/
/***********************************************************************************************************************
* File Name    : bsp_eeprom.c
* Description  : This module calls the id eeprom data read frm the Board eeprom, this is required for the board
*              : specific initialization.
***********************************************************************************************************************/

/*******************************************************************************************************************//**
 * @addtogroup BSP_BOARD_DK2M_INIT
 *
 * @{
 **********************************************************************************************************************/

/***********************************************************************************************************************
Includes   <System Includes> , "Project Includes"
***********************************************************************************************************************/
#include "bsp_api.h"
#include "bsp_sci_i2c.h"
#include "bsp_eeprom.h"


/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
#if defined(BSP_BOARD_S7G2_DK)

/***********************************************************************************************************************
Typedef definitions
***********************************************************************************************************************/

/***********************************************************************************************************************
Exported global variables (to be accessed by other files)
***********************************************************************************************************************/
 
/***********************************************************************************************************************
Private global variables and functions
***********************************************************************************************************************/

/*******************************************************************************
* Function name : bsp_s7g2_dk_id_read
* Description   : Reads the ID code from the I2C based Board EEPROM
* Arguments     : None

* Return value  : Return 0 or 0xFF
*******************************************************************************/
uint8_t bsp_s7g2_dk_id_read(void)
{

    uint8_t w_cmddata[34];
    uint8_t w_addr[2];

    /* Enable PFS access */
    /* Enable writing to PFSWE bit */
    R_PMISC->PWPR_b.BOWI             = 0x0;
    /* Enable writing to the PFS register */
    R_PMISC->PWPR_b.PFSWE            = 0x1;


    /* Setup SCI7 ports to use as simple I2C */
    R_PFS->PA02PFS_b.PMR             = 0x1;
    R_PFS->PA03PFS_b.PMR             = 0x1;

    /* SCL7, SDA7 */
    R_PFS->PA02PFS_b.PSEL            = 0x5;
    R_PFS->PA03PFS_b.PSEL            = 0x5;

    /* Disable PFS access */
    /* Enable writing to PFSWE bit */
    R_PMISC->PWPR_b.BOWI             = 0x0;
    /* Disable writing to the PFS register */
    R_PMISC->PWPR_b.PFSWE            = 0x0;
    /* Disable writing to PFSWE bit */
    R_PMISC->PWPR_b.BOWI             = 0x1;

    /* Enable clock to SCI7 */
    R_MSTP->MSTPCRB_b.MSTPB24        = 0U;

    /* Initialize IIC */
    bsp_init_sciiic();

    /* EEPROM's internal address */
    w_cmddata[0]                    = 0x0;
    w_cmddata[1]                    = 0x0;

    for(uint8_t i=0; i<32; i++)
    {
        w_cmddata[2+i]              = 0x0;
    }
    /* EEPROM's internal address */
    w_addr[0]                    = 0x0;
    w_addr[1]                    = 0x0;

    /* read data */
    bsp_sciiic_rx(w_addr, w_cmddata, 16);
    bsp_sciiic_clear();
    /* If no I2C device on the board it will return 0xFF */
    /* DK-S7G2 V4.0 onwards has the I2C Board EEPROM to distinguish between older versions */
    if((0xFF == w_cmddata[0]) && (0xFF == w_cmddata[1]))
    {
        return 0xFF;
    }
    else
    {
        return 0;
    }
}
#endif


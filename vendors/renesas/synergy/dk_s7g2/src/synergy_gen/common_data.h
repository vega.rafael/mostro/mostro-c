/* generated common header file - do not edit */
#ifndef COMMON_DATA_H_
#define COMMON_DATA_H_
#include <stdint.h>
#include "bsp_api.h"
#include "r_dac.h"
#include "r_dac_api.h"
#include "r_gpt.h"
#include "r_timer_api.h"
#include "r_dtc.h"
#include "r_transfer_api.h"
#include "sf_audio_playback_hw_dac.h"
#include "sf_message.h"
#include "sf_message_payloads.h"
#include "sf_audio_playback.h"
#include "r_ioport.h"
#include "r_ioport_api.h"
#include "r_elc.h"
#include "r_elc_api.h"
#include "r_cgc.h"
#include "r_cgc_api.h"
#include "r_fmi.h"
#include "r_fmi_api.h"
#ifdef __cplusplus
extern "C"
{
#endif
/** DAC on DAC Instance. */
extern const dac_instance_t g_dac;
/** Timer on GPT Instance. */
extern const timer_instance_t g_timer_audio;
#ifndef NULL
void NULL(timer_callback_args_t *p_args);
#endif
/* Transfer on DTC Instance. */
extern const transfer_instance_t g_transfer_audio;
#ifndef NULL
void NULL(transfer_callback_args_t *p_args);
#endif
extern const sf_audio_playback_hw_instance_t g_sf_audio_playback_hw;
extern void g_message_init(void);

/* SF Message on SF Message Instance. */
extern const sf_message_instance_t g_sf_message;
void g_sf_message_err_callback(void *p_instance, void *p_data);
void sf_message_init(void);
/** IOPORT Instance */
extern const ioport_instance_t g_ioport;
/** ELC Instance */
extern const elc_instance_t g_elc;
/** CGC Instance */
extern const cgc_instance_t g_cgc;
/** FMI on FMI Instance. */
extern const fmi_instance_t g_fmi;
void g_common_init(void);
#ifdef __cplusplus
} /* extern "C" */
#endif
#endif /* COMMON_DATA_H_ */

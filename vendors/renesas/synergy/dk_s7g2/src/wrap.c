/**
 * Brief description of the file.
 * A more detailed descriotion, which can span several lines.
 * Should be using JAVADOC_AUDOBRIEF set to YES.
 *
 * @file    module.hc
 * @author  JaaC
 * @version V0.0
 * @date    01/01/2016
 *
 * @par
 *
 * SMART TECHNOLOGY GROUP S.A.S. SHALL NOT BE HELD LIABLE FOR ANY DIRECT,
 * INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM
 * THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
 * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
 *
 * <h2><center>&copy; COPYRIGHT 2016 Smart Technology Group S.A.S. </center></h2>
 */
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "wrap.h"

#ifdef WRAP_USE_THREADX
#include <tx_api.h>
#endif

#define WRAP_CNT_ENTRIES (256)
#ifdef WRAP_USE_THREADX
#define WRAP_HEAP_SIZE   (8 * 1024 * 1024)
#else
#define WRAP_HEAP_SIZE   (256 * 1024)
#endif

typedef struct
{
    void**  p_ptr;   //!< Pointer to array of pointers
    size_t* p_size;  //!< Pointer to array of sizes

} wrap_info_entry_t;

typedef struct
{
    wrap_info_entry_t* p_info;   //!< Entry information structure
    uint32_t           len;      //!< Length of array (number of entries)
    uint32_t           cnt;      //!< Index of current entry in array
    size_t             size_tot; //!< Totally used size

} wrap_info_array_t;

#ifdef WRAP_USE_THREADX
static TX_BYTE_POOL      g_byte_pool;
#else
static uint8_t           g_heap_buf[WRAP_HEAP_SIZE];
static uint32_t          g_heap_start;
static size_t            g_heap_idx;
#endif
static uint32_t          g_heap_start;
static void*             g_array_ptr[WRAP_CNT_ENTRIES];
static size_t            g_array_size[WRAP_CNT_ENTRIES];
static bool              g_is_init;

static wrap_info_entry_t g_array_entry;
static wrap_info_array_t g_array_info;

static bool wrap_find_slot (uint32_t* const p_slot);

static bool wrap_lookup_ptr (const void* const ptr,
                             uint32_t* const p_slot);

static bool wrap_alloc (void** const p_ptr,
                        size_t size);

static bool wrap_free (void* const ptr);

static uint32_t _cnt_pass = 0;

/**
 * A brief description of the function's purposes.
 * @param a A description for a.
 * @param b A description for b.
 * @return int8_t
 * @note An important note.
 * Detailed description of the function's purposes.
 */
void *__wrap_malloc (size_t size)
{
    void*    ptr = NULL;
    uint32_t idx;

    _cnt_pass++;

    if (65 == _cnt_pass)
    {
        _cnt_pass = 0;
    }

    /* Find a slot where to enter the pointer */
    if (g_is_init && (size > 0) && wrap_find_slot(&idx))
    {
        if (wrap_alloc(&ptr, size))
        {
            if (NULL != ptr)
            {
                /* Housekeeping */
                g_array_info.p_info->p_ptr[idx]  = ptr;
                g_array_info.p_info->p_size[idx] = size;
                g_array_info.size_tot           += size;
                g_array_info.cnt++;
            }
        }
    }

    return ptr;
}

/**
 * A brief description of the function's purposes.
 * @param a A description for a.
 * @param b A description for b.
 * @return int8_t
 * @note An important note.
 * Detailed description of the function's purposes.
 */
void *__wrap___builtin_alloca (size_t size)
{
    return __wrap_malloc(size);
}

/**
 * A brief description of the function's purposes.
 * @param a A description for a.
 * @param b A description for b.
 * @return int8_t
 * @note An important note.
 * Detailed description of the function's purposes.
 */
void *__wrap_realloc (void* ptr,
                      size_t size)
{
    void*    ptr_new = NULL;
    uint32_t idx;
    size_t   size_old;
    size_t   size_cpy;

    /* Lookup for the pointer */
    if (g_is_init && (size > 0) && wrap_lookup_ptr(ptr, &idx))
    {
        size_old = g_array_info.p_info->p_size[idx];

        if (size_old > 0)
        {
            ptr_new = __wrap_malloc(size);

            if (NULL != ptr_new)
            {
                size_cpy = size < size_old ? size : size_old;
                memcpy(ptr_new, ptr, size_cpy);
                __wrap_free(ptr);
            }
        }
    }

    return ptr_new;
}

/**
 * A brief description of the function's purposes.
 * @param a A description for a.
 * @param b A description for b.
 * @return int8_t
 * @note An important note.
 * Detailed description of the function's purposes.
 */
void __wrap_free (void* ptr)
{
    uint32_t idx;

    /* Lookup for the pointer */
    if (g_is_init && wrap_lookup_ptr(ptr, &idx))
    {
        if (wrap_free(ptr))
        {
            /* Housekeeping */
            g_array_info.size_tot           -= g_array_info.p_info->p_size[idx];
            g_array_info.p_info->p_ptr[idx]  = NULL;
            g_array_info.p_info->p_size[idx] = 0;
            g_array_info.cnt--;
        }
    }
}

/**
 * A brief description of the function's purposes.
 * @param a A description for a.
 * @param b A description for b.
 * @return int8_t
 * @note An important note.
 * Detailed description of the function's purposes.
 */
void wrap_init (void)
{
#ifdef WRAP_USE_THREADX

    /* Set up heap buffer */
    g_heap_start = 0x90000000;
#else

    /* Set up heap buffer */
    memset((void*)g_heap_buf, 0xCE, sizeof(g_heap_buf));
    g_heap_start = (uint32_t)(&g_heap_buf);
    g_heap_idx   = 0;
#endif

    /* Clear arrays */
    memset((void*)g_array_ptr, 0, sizeof(g_array_ptr));
    memset((void*)g_array_size, 0, sizeof(g_array_size));

    /* Initialize entry structure */
    g_array_entry.p_ptr  = g_array_ptr;
    g_array_entry.p_size = g_array_size;

    /* Initialize array information structure */
    g_array_info.p_info   = &g_array_entry;
    g_array_info.len      = WRAP_CNT_ENTRIES;
    g_array_info.cnt      = 0;
    g_array_info.size_tot = 0;

#ifdef WRAP_USE_THREADX
    if (TX_SUCCESS == tx_byte_pool_create(&g_byte_pool,
                                          "Heap Byte Memory Pool",
                                          (void*)g_heap_start,
                                          WRAP_HEAP_SIZE))
#endif
    {
        g_is_init = true;
    }
}

/**
 * A brief description of the function's purposes.
 * @param a A description for a.
 * @param b A description for b.
 * @return int8_t
 * @note An important note.
 * Detailed description of the function's purposes.
 */
uint32_t wrap_get_cnt (void)
{
    return g_array_info.cnt;
}

/**
 * A brief description of the function's purposes.
 * @param a A description for a.
 * @param b A description for b.
 * @return int8_t
 * @note An important note.
 * Detailed description of the function's purposes.
 */
uint32_t wrap_get_size_tot (void)
{
    return g_array_info.size_tot;
}

/**
 * A brief description of the function's purposes.
 * @param a A description for a.
 * @param b A description for b.
 * @return int8_t
 * @note An important note.
 * Detailed description of the function's purposes.
 */
static bool wrap_find_slot (uint32_t* const p_slot)
{
    bool     res = false;
    uint32_t idx;

    if ((NULL != p_slot) && (g_array_info.cnt < g_array_info.len - 1u))
    {
        for (idx = 0 ; (!res) && (idx < g_array_info.len) ; idx++)
        {
            if (NULL == g_array_info.p_info->p_ptr[idx])
            {
                *p_slot = idx;
                res     = true;
            }
        }
    }

    return res;
}

/**
 * A brief description of the function's purposes.
 * @param a A description for a.
 * @param b A description for b.
 * @return int8_t
 * @note An important note.
 * Detailed description of the function's purposes.
 */
static bool wrap_lookup_ptr (const void* const ptr,
                             uint32_t* const p_slot)
{
    bool     res = false;
    uint32_t idx;

    if (NULL != p_slot)
    {
        for (idx = 0 ; (!res) && (idx < g_array_info.len) ; idx++)
        {
            if (ptr == g_array_info.p_info->p_ptr[idx])
            {
                *p_slot = idx;
                res     = true;
            }
        }
    }

    return res;
}

/**
 * A brief description of the function's purposes.
 * @param a A description for a.
 * @param b A description for b.
 * @return int8_t
 * @note An important note.
 * Detailed description of the function's purposes.
 */
static bool wrap_alloc (void** const p_ptr,
                        size_t size)
{
    bool res = false;

#ifdef WRAP_USE_THREADX
    if (TX_SUCCESS == tx_byte_allocate(&g_byte_pool, p_ptr, size, TX_NO_WAIT))
    {
        res = true;
    }

#else
    if ((g_heap_idx + size) < WRAP_HEAP_SIZE)
    {
        *p_ptr      = &(((uint8_t*)(g_heap_start))[g_heap_idx]);
        g_heap_idx += size;
        res         = true;
    }
#endif

    return res;
}

/**
 * A brief description of the function's purposes.
 * @param a A description for a.
 * @param b A description for b.
 * @return int8_t
 * @note An important note.
 * Detailed description of the function's purposes.
 */
static bool wrap_free (void* const ptr)
{
    bool res = false;

#ifdef WRAP_USE_THREADX
    if (TX_SUCCESS == tx_byte_release(ptr))
    {
        res = true;
    }

#else
    (void)ptr;    // Avoid compiler warning
    res = true;
#endif

    return res;
}

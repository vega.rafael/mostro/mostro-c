/**
 * Brief description of the file.
 * A more detailed description, which can span several lines.
 * Should be using JAVADOC_AUDOBRIEF set to YES.
 *
 * @file    module.hc
 * @author  Jaime Aranguren
 * @version V0.0
 * @date    01/01/2020
 *
 * @par
 *
 * OUTER SPACE SOUNDS SHALL NOT BE HELD LIABLE FOR ANY DIRECT,
 * INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM
 * THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
 * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
 *
 * <h2><center>&copy; COPYRIGHT 2020 Outer Space Sounds </center></h2>
 */
#include <audio_thread.h>
#include "Heavy_synth.h"
#include "wrap.h"

#define SAMPLE_RATE_HZ (48000u) //!< Sampling rate in Hz
#define BLOCK_LEN      (256u)   //!< Block length in samples
#define CHANNELS_HW    (1u)     //!< Number of audio channels in the HW side (DAC)
#define CHANNELS_SW    (2u)     //!< Number of audio channels in the SW side (patch)

/* Audio Thread entry function */
void audio_thread_entry (void)
{
    float                     process_buffer[BLOCK_LEN * CHANNELS_SW] = { 0.0 };       // Buffer for storing processed samples
    int16_t                   output_buffer[BLOCK_LEN * CHANNELS_HW]  = { 0 };         // Buffer for storing output samples
    sf_message_acquire_cfg_t  cfg_acquire                             = { .buffer_keep = false };
    sf_audio_playback_data_t* p_playback_data                         = NULL;
    sf_audio_playback_data_t  ref_playback_data;
    bsp_leds_t                leds;
    HeavyContextInterface*    synth;
    uint16_t cnt_i ;

#ifdef USE_WRAP

    /* Initialize memory management wrapper */
    wrap_init();
#endif

    /* Get LED information for this board */
    R_BSP_LedsGet(&leds);

    // Initialize playback data
    ref_playback_data.header.event_b.class_code     = SF_MESSAGE_EVENT_CLASS_AUDIO;
    ref_playback_data.header.event_b.code           = SF_MESSAGE_EVENT_AUDIO_START;
    ref_playback_data.header.event_b.class_instance = 0;
    ref_playback_data.type.is_signed                = 1;
    ref_playback_data.type.scale_bits_max           = 16;
    ref_playback_data.loop_timeout                  = TX_NO_WAIT;

    /* Initialize synthesizer object */
    synth = hv_synth_new(SAMPLE_RATE_HZ);

    while (1)
    {
        /* Wait until buffer is available */
        tx_semaphore_get(&g_audio_semaphore, TX_WAIT_FOREVER);

        /* Measurement on */
        g_ioport.p_api->pinWrite(leds.p_leds[3], IOPORT_LEVEL_HIGH);

        /* Synthesizer processing */
        hv_processInlineInterleaved(synth, NULL, process_buffer, BLOCK_LEN);

        /* Measurement off */
        g_ioport.p_api->pinWrite(leds.p_leds[3], IOPORT_LEVEL_LOW);

        // Range conversion, to 12 bits
        for (cnt_i = 0 ; cnt_i < BLOCK_LEN ; ++cnt_i)
        {
            int16_t out_sample = (int16_t)((process_buffer[cnt_i * CHANNELS_SW] * INT16_MAX));
            output_buffer[cnt_i] = out_sample;
        }

        /* Acquire buffer for Audio Playback Message */
        if (SSP_SUCCESS == g_sf_message.p_api->bufferAcquire(g_sf_message.p_ctrl,
                                                             (sf_message_header_t**)&p_playback_data,
                                                             &cfg_acquire,
                                                             TX_WAIT_FOREVER))
        {
            /* Write playback data to the message buffer */
            *p_playback_data            = ref_playback_data;
            p_playback_data->p_data     = output_buffer;
            p_playback_data->stream_end = false;
            p_playback_data->size_bytes = sizeof(output_buffer);

            /* Play the buffer */
            g_sf_audio_playback.p_api->start(g_sf_audio_playback.p_ctrl, p_playback_data, TX_NO_WAIT);
        }
    }
}

/**
 * A brief description of the function's purposes.
 * @param a A description for a.
 * @param b A description for b.
 * @return int8_t
 * @note An important note.
 * Detailed description of the function's purposes.
 */
void audio_thread_cb (sf_message_callback_args_t* p_args)
{
    SSP_PARAMETER_NOT_USED(p_args);
    tx_semaphore_put(&g_audio_semaphore);
}

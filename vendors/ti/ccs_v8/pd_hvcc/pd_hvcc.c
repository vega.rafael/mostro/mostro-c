/***********************************************************************************************************************
*  Includes   <System Includes> , "Project Includes"
***********************************************************************************************************************/
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include <malloc.h>
#include <errno.h>
#include "oss_mostro.h"
#include "soc_AM335x.h"
#include "gpio_v2.h"
#include "consoleUtils.h"
#include "ascii.h"
#include "interrupt.h"
#include "hsi2c.h"
#include "trace.h"
#include "fifo.h"
#include "disp.h"
#include "bsp.h"
#include "Heavy_synth.h"

/***********************************************************************************************************************
*  Macro definitions
***********************************************************************************************************************/
#define ITER_CNT_MAX      (0x00001000u)                   //!< Iteration max counter for low speed processing
#define ADC_CNT_MAX       (0x00000400u)                   //!< Iteration max counter for ADC processing
#define AUDIO_FIFO_LEN    (1024)                          //!< Audio FIFO length
#define MIDI_FIFO_LEN     (16)                            //!< MIDI FIFO length

#define SAMPLE_RATE_HZ    (48000u)                        //!< Sample rate in Hz
#define SAWTOOTH_OUT_STEP (10u)                           //!< Step to increment sawtooth sample
#define SINE_FREQ_HZ      (480u)                          //!< Sinewave frequency in Herz
#define SINE_W0           (2.0f * (float)M_PI * ((float)SINE_FREQ_HZ / (float)BSP_AUDIO_SAMPLE_RATE))
#define SINE_CNT_SAMPLES  (SAMPLE_RATE_HZ / SINE_FREQ_HZ) //!< Number of samples for a full cycle of sinewave
#define BLOCK_SIZE        (256u)                          //!< Block size in number of samples
#define CHANNELS_HW       (1u)                            //!< Number of audio channels in the HW side (DAC)
#define CHANNELS_SW       (2u)                            //!< Number of audio channels in the SW side (patch)
#define BUFF_LEN          (BLOCK_SIZE * CHANNELS_SW)      //!< Buffer length, accounting for number of channels in patch

#ifndef BSP_USE_AUDIO_DMA
#undef USE_PASSTHRU_OUT                                   //!< Define if want to use passthru audio, undefine otherwise (only for interrupt based audio)
#endif                                                    // BSP_USE_AUDIO_DMA

#undef USE_MIDI_LOOP                                      /* Define if you want to echo out incoming midi messages instead of
                                                           * sending messages defined here. Useful for testing MIDI i/o with
                                                           * another MIDI device. */

#undef USE_MEASURE_ONLY                                   /* Define for only measuring the processing time on BSP_LED_AMP,
                                                           * which toggles with every processing pass, undefine for normal processing */

#undef USE_SIM_ONLY                                       // Define if generating executable for simulator only, undefine for actual execution on HW

#ifdef USE_SIM_ONLY
#define USE_MEASURE_ONLY                                  // Override for simulation
#endif                                                    // USE_SIM_ONLY

#ifndef M_PI
#define M_PI             (3.14159265358979323846)
#endif    // M_PI

/***********************************************************************************************************************
*  Typedef definitions
***********************************************************************************************************************/

/* @enum audio_l_t
 * @brief Enumeration for audio left channel */
typedef enum
{
    AUDIO_L_SIG,      //!< Audio left channel output: internally generated signal
    AUDIO_L_CV_GATE,  //!< Audio left channel output: CV gate
    AUDIO_L_CV_PITCH, //!< Audio left channel output: CV pitch
    AUDIO_L_TOTAL     //!< Total audio left channel output options

} audio_l_t;

/* @enum audio_sig_t
 * @brief Enumeration for audio signals */
typedef enum
{
    AUDIO_SIG_SINE,    //!< Sinusoidal signal
    AUDIO_SIG_SYNTH,   //!< Synthesizer signal
    AUDIO_SIG_TOTAL    //!< Total audio signal types

} audio_sig_t;

/***********************************************************************************************************************
*  Imported global variables (from other files)
***********************************************************************************************************************/

/***********************************************************************************************************************
*  Exported global variables (to be accessed by other files)
***********************************************************************************************************************/

/***********************************************************************************************************************
*  Private global variables
***********************************************************************************************************************/
static HeavyContextInterface* g_synth;            //!< Synthesizer object

#ifndef BSP_USE_AUDIO_DMA
static int16_t   g_audio_sample_l;                //!< Audio sample, left channel
static int16_t   g_audio_sample_r;                //!< Audio sample, right channel
static audio_l_t g_audio_l_sel;                   //!< Audio left channel selection
#endif                                            // BSP_USE_AUDIO_DMA

static bsp_adc_info_t g_adc_data;                 //!< ADC sample
static bool           g_adc_busy;                 //!< ADC busy flag
static uint8_t        g_midi_buff[MIDI_FIFO_LEN]; //!< MIDI buffer
static fifo_8_t       g_midi_fifo;                //!< MIDI FIFO
static int16_t*       gp_audio_out;               //!< Pointer to buffer for data to send out
static bool           g_audio_out_is_a;           //!< True if audio output buffer is buffer A, false otherwise
static bool           g_audio_out_done;           //!< True when audio tx handler is done sending the data in gp_audio_out
float                 g_buf_process[BUFF_LEN];    //!< Audio processing buffer

/***********************************************************************************************************************
*  Private function prototypes
***********************************************************************************************************************/

/* Event handlers */
static void hnd_audio_tx (void* const p_param);

static void hnd_audio_rx (void* const p_param);

static void hnd_adc (void* const p_param);

static void hnd_midi_rx (void* const p_param);

static void led_off_all (void);

static void led_off_app (void);

static void led_on_app (void);

static bool key_any_on (void);

static bool key_any_pressed (void);

static bool but_any_on (void);

static bool but_any_pressed (void);

static void adc_process (void);

static void midi_loop (void);

static void fatal_error (const char* const p_reason_str);

static void disp_adc_decimal (uint16_t data,
                              const bool force_update);

static void adc_process_disp (const bsp_adc_tag_t tag,
                              const bsp_mux_ch_t ch,
                              const bool force_update);

static void blink_alive (void);

static void housekeep (void);

static void audio_block (int16_t* const p_buf,
                         const audio_sig_t sig);

/***********************************************************************************************************************
*  Public function bodies
***********************************************************************************************************************/

int posix_memalign (void** memptr,
                    size_t alignment,
                    size_t size)
{
    int   ret;
    void* p = NULL;

    p = memalign(alignment, size);

    if (NULL != p)
    {
        *memptr = p;
    }
    else
    {
        ret = EINVAL;
    }

    return ret;
}


/**
 * A brief description of the function's purposes.
 * @param a A description for a.
 * @param b A description for b.
 * @return int8_t
 * @note An important note.
 * Detailed description of the function's purposes.
 */
int main ()
{
    int16_t     buf_a[BLOCK_SIZE] = { 0 };
    int16_t     buf_b[BLOCK_SIZE] = { 0 };
    audio_sig_t audio_sig         = AUDIO_SIG_SYNTH;

#ifdef USE_MEASURE_ONLY
    bool led_on = false;
#endif

    /* Initialize data */
    g_audio_l_sel    = AUDIO_L_SIG;
    g_audio_out_is_a = false;
    g_audio_out_done = false;
    gp_audio_out     = buf_b;

    /* Synthesizer object */
    g_synth = hv_synth_new(SAMPLE_RATE_HZ);

#ifndef USE_SIM_ONLY

    /* Initialize BSP */
    bsp_init(hnd_audio_tx, hnd_audio_rx, hnd_adc, hnd_midi_rx);

    led_off_all();
    ConsoleUtilsPrintf("\n");
    ConsoleUtilsPrintf("************************************\n");
    ConsoleUtilsPrintf("OSS - MostroV3 PD via HVCC benchmark\n");
#ifdef BSP_USE_AUDIO_DMA
    ConsoleUtilsPrintf("          DMA based audio          \n");
#else
    ConsoleUtilsPrintf("       Interrupt based audio       \n");
#endif    // BSP_USE_AUDIO_DMA
    ConsoleUtilsPrintf("************************************\n");
#endif    // USE_SIM_ONLY

    if (FIFO_RESULT_SUCCESS != fifo_8_create(g_midi_buff, MIDI_FIFO_LEN, &g_midi_fifo))
    {
        fatal_error("fifo_8_create() failed");
    }
    else
    {
#ifndef USE_SIM_ONLY

        /* Any other processing here */
        ConsoleUtilsPrintf("\n");
#endif    // USE_SIM_ONLY

        while (1)
        {
#ifndef USE_SIM_ONLY

            /* Avoid WDT reset */
            housekeep();
#endif      // USE_SIM_ONLY

            /* Check if audio handler is done with sending data */
            if (g_audio_out_done)
            {
                /* Clear flag */
                g_audio_out_done = false;

#ifndef USE_MEASURE_ONLY
                if (g_audio_out_is_a)
                {
                    /* Quickly toggle output buffer */
                    gp_audio_out     = buf_b;
                    g_audio_out_is_a = false;

                    /* Create audio block in the other buffer */
                    audio_block(buf_a, audio_sig);
                }
                else
                {
                    /* Quickly toggle output buffer */
                    gp_audio_out     = buf_a;
                    g_audio_out_is_a = true;

                    /* Create audio block in the other buffer */
                    audio_block(buf_b, audio_sig);
                }
#endif          //USE_MEASURE_ONLY
            }

#ifdef USE_MEASURE_ONLY

            /* Process a block of data */
            audio_block(buf_a, audio_sig);

            /* Toggle led to measure processing time */
            if (led_on)
            {
                bsp_led_on(BSP_LED_AMP);
                led_on = false;
            }
            else
            {
                bsp_led_off(BSP_LED_AMP);
                led_on = true;
            }
#endif      // USE_MEASURE_ONLY

            /* Turn leds on with key */
            if (key_any_pressed())
            {
                bsp_led_on(BSP_LED_ENV_FB);
                audio_sig = AUDIO_SIG_SYNTH;
            }

            /* Turn leds off with button */
            if (but_any_pressed())
            {
                bsp_led_off(BSP_LED_ENV_FB);
                audio_sig = AUDIO_SIG_SINE;
            }
        }
    }

    return 0;
}

/***********************************************************************************************************************
*  Private function bodies
***********************************************************************************************************************/

/**
 * Handler for audio transmit interrupt.
 * @param p_param Pointer to the location where to store the sample to be transmitted.
 * It obtains the sample to be transmitted and stores it into a given pointer (p_param).
 * The interrupt happens once per channel.
 * The variable "flag" toggles for switching between left and right channels.
 */
static void hnd_audio_tx (void* const p_param)
{
    static bool flag = false;

#ifndef USE_PASSTHRU_OUT
    static int16_t  audio_sample = 0;
    static uint32_t cnt_sample   = 0;
#endif

    /* Toggle */
    if (flag)
    {
        flag = false;
        bsp_led_off(BSP_LED_USER_2);

#ifndef BSP_USE_AUDIO_DMA
#ifndef USE_PASSTHRU_OUT
        switch (g_audio_l_sel)
        {
            case AUDIO_L_CV_GATE:

                /* Use CV gate sample (right channel) */
                *((int16_t*)(p_param)) = g_audio_sample_r;
                break;

            case AUDIO_L_CV_PITCH:

                /* Use CV pitch sample (left channel) */
                *((int16_t*)(p_param)) = g_audio_sample_l;
                break;

            default:

                /* Use sawtooth sample */
                *((int16_t*)(p_param)) = audio_sample;
                break;
        }

#else    // USE_PASSTHRU_OUT

        /* Get from right channel sample */
        *((int16_t*)(p_param)) = g_audio_sample_r;
#endif    // USE_PASSTHRU_OUT
#endif    // BSP_USE_AUDIO_DMA
    }
    else
    {
        flag = true;
        bsp_led_on(BSP_LED_USER_2);

#ifndef BSP_USE_AUDIO_DMA
#ifndef USE_PASSTHRU_OUT
        switch (g_audio_l_sel)
        {
            case AUDIO_L_CV_GATE:

                /* Use CV gate sample (right channel) */
                *((int16_t*)(p_param)) = g_audio_sample_r;
                break;

            case AUDIO_L_CV_PITCH:

                /* Use CV pitch sample (left channel) */
                *((int16_t*)(p_param)) = g_audio_sample_l;
                break;

            default:

                /* Use sawtooth sample */
                *((int16_t*)(p_param)) = audio_sample;
                break;
        }

        /* Get next audio sample */
        audio_sample = gp_audio_out[cnt_sample++];

        /* If got to end of block, wrap around and set flag */
        if (cnt_sample >= BLOCK_SIZE)
        {
            cnt_sample       = 0;
            g_audio_out_done = true;
        }

#else    // USE_PASSTHRU_OUT

        /* Get from left channel sample */
        *((int16_t*)(p_param)) = g_audio_sample_l;
#endif    // USE_PASSTHRU_OUT
#endif    // BSP_USE_AUDIO_DMA
    }
}

/**
 * Handler for audio receive interrupt.
 * @param p_param Pointer to the location holding the received sample.
 * It captures the received audio sample into global variables g_audio_sample_l and g_audio_sample_r.
 * The interrupt happens once per channel.
 * The variable "flag" toggles for switching between left and right channels.
 */
static void hnd_audio_rx (void* const p_param)
{
    static bool flag = false;

    /* Toggle */
    if (flag)
    {
        flag = false;
        bsp_led_off(BSP_LED_USER_3);

#ifndef BSP_USE_AUDIO_DMA

        /* Put into left channel sample */
        g_audio_sample_l = *((int16_t*)(p_param));
#endif    // BSP_USE_AUDIO_DMA
    }
    else
    {
        flag = true;
        bsp_led_on(BSP_LED_USER_3);

#ifndef BSP_USE_AUDIO_DMA

        /* Put into right channel sample */
        g_audio_sample_r = *((int16_t*)(p_param));
#endif    // BSP_USE_AUDIO_DMA
    }
}

/**
 * Handler for ADC interrupt.
 * @param p_param Pointer to the location holding the received data.
 * It captures the received data into global variable g_adc_data.
 */
static void hnd_adc (void* const p_param)
{
    TRACE_FUNC_CALL_IN();

    /* Store into local buffer */
    g_adc_data.sample = ((bsp_adc_info_t*)p_param)->sample;
    g_adc_data.tag    = ((bsp_adc_info_t*)p_param)->tag;

    /* Clear busy flag */
    g_adc_busy = false;

    TRACE_FUNC_CALL_OUT();
}

/**
 * Handler for MIDI interrupt.
 * @param p_param Pointer to the location holding the received data.
 * It pushes the received data into global FIFO variable g_midi_fifo.
 */
static void hnd_midi_rx (void* const p_param)
{
    TRACE_FUNC_CALL_IN();

    /* Store into FIFO */
    fifo_8_push(*(uint8_t*)p_param, &g_midi_fifo);

    TRACE_FUNC_CALL_OUT();
}

/**
 * Turn all LEDs off, including BBx user LEDs.
 */
static void led_off_all (void)
{
    TRACE_FUNC_CALL_IN();

    bsp_led_off(BSP_LED_USER_0);
    bsp_led_off(BSP_LED_USER_1);
    bsp_led_off(BSP_LED_USER_2);
    bsp_led_off(BSP_LED_USER_3);
    bsp_led_off(BSP_LED_ENV_RATIO);
    bsp_led_off(BSP_LED_ENV_INDEX);
    bsp_led_off(BSP_LED_ENV_FB);
    bsp_led_off(BSP_LED_SUB);
    bsp_led_off(BSP_LED_AMP);
    bsp_led_off(BSP_LED_SUS);
    bsp_led_off(BSP_LED_LFO_RATIO);
    bsp_led_off(BSP_LED_LFO_INDEX);
    bsp_led_off(BSP_LED_LFO_FB);
    bsp_led_off(BSP_LED_LFO_PITCH);
    bsp_led_off(BSP_LED_LOOP);

    TRACE_FUNC_CALL_OUT();
}

/**
 * Turn off all of the LEDs that are relevant for the application.
 */
static void led_off_app (void)
{
    TRACE_FUNC_CALL_IN();

    bsp_led_off(BSP_LED_ENV_RATIO);
    bsp_led_off(BSP_LED_ENV_INDEX);
    bsp_led_off(BSP_LED_ENV_FB);
    bsp_led_off(BSP_LED_SUB);
    bsp_led_off(BSP_LED_AMP);
    bsp_led_off(BSP_LED_SUS);
    bsp_led_off(BSP_LED_LFO_RATIO);
    bsp_led_off(BSP_LED_LFO_INDEX);
    bsp_led_off(BSP_LED_LFO_FB);
    bsp_led_off(BSP_LED_LFO_PITCH);
    bsp_led_off(BSP_LED_LOOP);

    TRACE_FUNC_CALL_OUT();
}

/**
 * Turn on  all of the LEDs that are relevant for the application.
 */
static void led_on_app (void)
{
    TRACE_FUNC_CALL_IN();

    bsp_led_on(BSP_LED_ENV_RATIO);
    bsp_led_on(BSP_LED_ENV_INDEX);
    bsp_led_on(BSP_LED_ENV_FB);
    bsp_led_on(BSP_LED_SUB);
    bsp_led_on(BSP_LED_AMP);
    bsp_led_on(BSP_LED_SUS);
    bsp_led_on(BSP_LED_LFO_RATIO);
    bsp_led_on(BSP_LED_LFO_INDEX);
    bsp_led_on(BSP_LED_LFO_FB);
    bsp_led_on(BSP_LED_LFO_PITCH);
    bsp_led_on(BSP_LED_LOOP);

    TRACE_FUNC_CALL_OUT();
}

/**
 * Check for key on.
 * @return true if any key is on.
 */
static bool key_any_on (void)
{
    bool ret = false;

    if ((bsp_key_get(BSP_KEY_1) == BSP_SW_ON) ||
        (bsp_key_get(BSP_KEY_2) == BSP_SW_ON) ||
        (bsp_key_get(BSP_KEY_3) == BSP_SW_ON) ||
        (bsp_key_get(BSP_KEY_4) == BSP_SW_ON) ||
        (bsp_key_get(BSP_KEY_5) == BSP_SW_ON) ||
        (bsp_key_get(BSP_KEY_6) == BSP_SW_ON) ||
        (bsp_key_get(BSP_KEY_7) == BSP_SW_ON) ||
        (bsp_key_get(BSP_KEY_8) == BSP_SW_ON) ||
        (bsp_key_get(BSP_KEY_9) == BSP_SW_ON) ||
        (bsp_key_get(BSP_KEY_10) == BSP_SW_ON) ||
        (bsp_key_get(BSP_KEY_11) == BSP_SW_ON) ||
        (bsp_key_get(BSP_KEY_12) == BSP_SW_ON) ||
        (bsp_key_get(BSP_KEY_13) == BSP_SW_ON))
    {
        ret = true;
    }

    return ret;
}

/**
 * Check for key being pressed.
 * @return true if any key is pressed.
 */
static bool key_any_pressed (void)
{
    bool ret = false;

    if (bsp_key_get_pressed(BSP_KEY_1) || bsp_key_get_pressed(BSP_KEY_2) ||
        bsp_key_get_pressed(BSP_KEY_3) || bsp_key_get_pressed(BSP_KEY_4) ||
        bsp_key_get_pressed(BSP_KEY_5) || bsp_key_get_pressed(BSP_KEY_6) ||
        bsp_key_get_pressed(BSP_KEY_7) || bsp_key_get_pressed(BSP_KEY_8) ||
        bsp_key_get_pressed(BSP_KEY_9) || bsp_key_get_pressed(BSP_KEY_10) ||
        bsp_key_get_pressed(BSP_KEY_11) ||
        bsp_key_get_pressed(BSP_KEY_12) ||
        bsp_key_get_pressed(BSP_KEY_13))
    {
        ret = true;
    }

    return ret;
}

/**
 * Check for button on.
 * @return true if any button is on.
 */
static bool but_any_on (void)
{
    bool ret = false;

    if ((bsp_but_get(BSP_BUT_DISP_P) == BSP_SW_ON) ||
        (bsp_but_get(BSP_BUT_DISP_M) == BSP_SW_ON) ||
        (bsp_but_get(BSP_BUT_ENV_3) == BSP_SW_ON) ||
        (bsp_but_get(BSP_BUT_SUB) == BSP_SW_ON) ||
        (bsp_but_get(BSP_BUT_AMP) == BSP_SW_ON) ||
        (bsp_but_get(BSP_BUT_SUS) == BSP_SW_ON) ||
        (bsp_but_get(BSP_BUT_CH_KEY) == BSP_SW_ON) ||
        (bsp_but_get(BSP_BUT_CH_SCALE) == BSP_SW_ON) ||
        (bsp_but_get(BSP_BUT_OCT_KEY) == BSP_SW_ON) ||
        (bsp_but_get(BSP_BUT_OCT_SCALE) == BSP_SW_ON) ||
        (bsp_but_get(BSP_BUT_TAP) == BSP_SW_ON) ||
        (bsp_but_get(BSP_BUT_LFO_4) == BSP_SW_ON) ||
        (bsp_but_get(BSP_BUT_LOOP) == BSP_SW_ON))
    {
        ret = true;
    }

    return ret;
}

/**
 * Check for button being pressed.
 * @return true if any button is pressed.
 */
static bool but_any_pressed (void)
{
    bool ret = false;

    if (bsp_but_get_pressed(BSP_BUT_DISP_P) ||
        bsp_but_get_pressed(BSP_BUT_DISP_M) ||
        bsp_but_get_pressed(BSP_BUT_ENV_3) ||
        bsp_but_get_pressed(BSP_BUT_SUB) ||
        bsp_but_get_pressed(BSP_BUT_AMP) ||
        bsp_but_get_pressed(BSP_BUT_SUS) ||
        bsp_but_get_pressed(BSP_BUT_CH_KEY) ||
        bsp_but_get_pressed(BSP_BUT_CH_SCALE) ||
        bsp_but_get_pressed(BSP_BUT_OCT_KEY) ||
        bsp_but_get_pressed(BSP_BUT_OCT_SCALE) ||
        bsp_but_get_pressed(BSP_BUT_TAP) ||
        bsp_but_get_pressed(BSP_BUT_LFO_4) ||
        bsp_but_get_pressed(BSP_BUT_LOOP))
    {
        ret = true;
    }

    return ret;
}

/**
 * Process ADC data.
 * Start new capture or print captured data to console.
 */
static void adc_process (void)
{
    static volatile bool waiting = false;
    static uint8_t       cnt     = 0;
    const bsp_adc_tag_t  tag[]   = { BSP_ADC_TAG_OSC_RATIO, BSP_ADC_TAG_OSC_INDEX,
                                     BSP_ADC_TAG_OSC_FB,       BSP_ADC_TAG_MUX };

    TRACE_FUNC_CALL_IN();

    if (!waiting)
    {
        /* Not waiting, set waiting and busy flags and start new capture */
        waiting    = true;
        g_adc_busy = true;
        bsp_mux_ch(BSP_MUX_CH_VOL);
        bsp_adc_start(tag[3]);

        /* Calculate next tag, with wrap around */
        cnt = (cnt + 1) % (sizeof(tag) / sizeof(bsp_adc_tag_t));
    }
    else
    {
        if (!g_adc_busy)
        {
            /* Was waiting but ADC is not busy anymore, process sample and clear flag */
            ConsoleUtilsPrintf("ADC -> tag: %d - sample: 0x%04x\n",
                               g_adc_data.tag, g_adc_data.sample);
            waiting = false;
        }
    }

    TRACE_FUNC_CALL_OUT();
}

/**
 * MIDI loop.
 * Pop data from MIDI FIFO and send it back via MIDI.
 */
static void midi_loop (void)
{
    uint8_t midi_data;

    /* Process any data available in the FIFO */
    while (FIFO_RESULT_SUCCESS == fifo_8_pop(&midi_data, &g_midi_fifo))
    {
        /* Show received data */
        ConsoleUtilsPrintf("MIDI Rx: 0x%02x\n", midi_data);

        /* Echo it back on the MIDI out port */
        bsp_midi_tx(midi_data);
    }
}

/**
 * Fatal error.
 * @param p_reason_str Pointer to a string explaining the reason.
 * Prints the reason for the error and enters an infinite loop toggling the BBx user LEDs.
 */
static void fatal_error (const char* const p_reason_str)
{
    volatile unsigned int iter_cnt = ITER_CNT_MAX >> 2;
    bool                  flag     = false;

    /* Print message */
    ConsoleUtilsPrintf("FATAL ERROR: %s\n", p_reason_str);

    /* Disable interrupts */
    bsp_int_dis();

    while (1)
    {
        /* Housekeeping */
        housekeep();

        /* Low speed processing */
        if (iter_cnt)
        {
            iter_cnt--;
        }
        else
        {
            /* Iteration counter expired. Do the processing */
            iter_cnt = ITER_CNT_MAX >> 2;    // Reload iteration counter

            /* Flag tick processing */
            if (flag)
            {
                /* Toggle */
                flag = false;
                bsp_led_on(BSP_LED_USER_0);
                bsp_led_on(BSP_LED_USER_1);
                bsp_led_on(BSP_LED_USER_2);
                bsp_led_on(BSP_LED_USER_3);
            }
            else
            {
                /* Toggle */
                flag = true;
                bsp_led_off(BSP_LED_USER_0);
                bsp_led_off(BSP_LED_USER_1);
                bsp_led_off(BSP_LED_USER_2);
                bsp_led_off(BSP_LED_USER_3);
            }
        }
    }
}

/**
 * Print 12-bit data as decimal in percentage.
 * @param data          Data to print in display.
 * @param force_update  true if the digits are forced to be updated.
 */
static void disp_adc_decimal (uint16_t data,
                              const bool force_update)
{
    static char disp1_data_prev = '0';
    static char disp2_data_prev = '0';
    char        disp1_data;
    char        disp2_data;

    /* Prepare values */
    data       = (uint8_t)((100 * data) >> 12);
    disp1_data = DigitToASCII(data / 10, BASE_DECIMAL);
    disp2_data = DigitToASCII(data % 10, BASE_DECIMAL);

    /* Update display only if needed */
    if ((disp1_data != disp1_data_prev) | force_update)
    {
        bsp_disp_send(BSP_DISP_1, disp1_data, BSP_DISP_DP_OP_OFF);
    }

    if ((disp2_data != disp2_data_prev) | force_update)
    {
        bsp_disp_send(BSP_DISP_2, disp2_data, BSP_DISP_DP_OP_OFF);
    }

    /* Update older values */
    disp1_data_prev = disp1_data;
    disp2_data_prev = disp2_data;
}

/**
 * Process and display ADC data.
 * Start new capture or display captured data in decimal format.
 */
static void adc_process_disp (const bsp_adc_tag_t tag,
                              const bsp_mux_ch_t ch,
                              const bool force_update)
{
    static volatile bool waiting = false;

    /* ADC processing */
    if (!waiting)
    {
        /* Not waiting, set waiting and busy flags and start new capture */
        waiting    = true;
        g_adc_busy = true;
        bsp_mux_ch(ch);
        bsp_adc_start(tag);
    }
    else
    {
        if (!g_adc_busy)
        {
            /* Correct signal inversion at input buffer */
            if ((BSP_ADC_TAG_CV_INDEX == tag) || (BSP_ADC_TAG_CV_FB == tag) ||
                (BSP_ADC_TAG_CV_RATIO == tag))
            {
                g_adc_data.sample = 4096u - g_adc_data.sample;
            }

            /* Show in display and restart */
            disp_adc_decimal(g_adc_data.sample, force_update);
            waiting = false;
        }
    }
}

/**
 * Blink two BBx user LEDs to indicate that the system is alive (heartbeat).
 */
static void blink_alive (void)
{
    static volatile uint32_t iter_cnt = ITER_CNT_MAX;
    static bool              flag     = false;

    /* Low speed processing to show that we are alive */
    if (iter_cnt)
    {
        iter_cnt--;
    }
    else
    {
        /* Iteration counter expired. Do the processing */
        iter_cnt = ITER_CNT_MAX;    // Reload iteration counter

        /* Flag tick processing */
        if (flag)
        {
            /* Toggle */
            flag = false;
            bsp_led_on(BSP_LED_USER_0);
            bsp_led_on(BSP_LED_USER_1);
        }
        else
        {
            /* Toggle */
            flag = true;
            bsp_led_off(BSP_LED_USER_0);
            bsp_led_off(BSP_LED_USER_1);
        }
    }
}

/**
 * Housekeeping.
 * It involves WDT clear and audio DMA processing (if enabled).
 */
static void housekeep (void)
{
    /* WDT clear */
    bsp_wdt_clr();

#ifdef BSP_USE_AUDIO_DMA
    bsp_edma_audio_process();
#endif    // BSP_USE_AUDIO_DMA
}

/**
 * A brief description of the function's purposes.
 * @param a A description for a.
 * @param b A description for b.
 * @return int8_t
 * @note An important note.
 * Detailed description of the function's purposes.
 */
static void audio_block (int16_t* const p_buf,
                         const audio_sig_t sig)
{
    static uint32_t cnt_sample = 0;
    uint32_t        idx;

    /* Entering signal processing */
    bsp_led_on(BSP_LED_SUB);

    /* Decode the signal type */
    switch (sig)
    {
        case AUDIO_SIG_SINE:

            /* Sinewave generation */
            for (idx = 0 ; idx < BLOCK_SIZE ; idx++)
            {
                p_buf[idx] = (int16_t)(INT16_MAX * sinf(SINE_W0 * (float)(cnt_sample)));

                /* Increment counter with wrap around */
                cnt_sample++;

                if (cnt_sample >= SINE_CNT_SAMPLES)
                {
                    cnt_sample = 0;
                }
            }

            break;

        case AUDIO_SIG_SYNTH:

            /* Synthesizer processing */
            hv_processInline(g_synth, NULL, g_buf_process, BLOCK_SIZE);

            /* Scaling */
            for (idx = 0 ; idx < BLOCK_SIZE ; idx++)
            {
                p_buf[idx] = (int16_t)(g_buf_process[idx] * (float)INT16_MAX);
            }

            break;

        default:

            /* Set to zero */
            memset((void*)p_buf, 0, BLOCK_SIZE * sizeof(int16_t));
            break;
    }

    /* Entering signal processing */
    bsp_led_off(BSP_LED_SUB);
}

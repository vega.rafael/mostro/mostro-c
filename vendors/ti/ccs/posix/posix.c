/***********************************************************************************************************************
*  Includes   <System Includes> , "Project Includes"
***********************************************************************************************************************/
#include <stdint.h>
#include <stdbool.h>
#include <malloc.h>
#include <errno.h>

/***********************************************************************************************************************
*  Macro definitions
***********************************************************************************************************************/

/***********************************************************************************************************************
*  Typedef definitions
***********************************************************************************************************************/

/***********************************************************************************************************************
*  Imported global variables (from other files)
***********************************************************************************************************************/

/***********************************************************************************************************************
*  Exported global variables (to be accessed by other files)
***********************************************************************************************************************/

/***********************************************************************************************************************
*  Private global variables
***********************************************************************************************************************/

/***********************************************************************************************************************
*  Private function prototypes
***********************************************************************************************************************/
static bool posix_is_pow2 (size_t n);

static bool posix_is_multiple (size_t n,
                               uint32_t val);

/***********************************************************************************************************************
*  Public function bodies
***********************************************************************************************************************/

/* posix_memalign.
 * Implementation based on https://www.gnu.org/software/libc/manual/html_node/Aligned-Memory-Blocks.html */
int posix_memalign (void** memptr,
                    size_t alignment,
                    size_t size)
{
    int   ret;
    void* p = NULL;

    if (!(posix_is_pow2(alignment) && posix_is_multiple(alignment, sizeof(void*))))
    {
        ret = EINVAL;
    }
    else
    {
        p = memalign(alignment, size);

        if (NULL == p)
        {
            ret = ENOMEM;
        }
        else
        {
            *memptr = p;
            ret     = 0;
        }
    }

    return ret;
}

/***********************************************************************************************************************
*  Private function bodies
***********************************************************************************************************************/

/**
 * Check if a value is power of two.
 * @param n Value to check.
 */
static bool posix_is_pow2 (size_t n)
{
    bool     res      = false;
    uint8_t  cnt_bits = sizeof(size_t) * 8u;
    uint64_t mask     = 1u;

    while (!res && cnt_bits--)
    {
        if (mask == n)
        {
            res = true;
        }
        else
        {
            mask <<= 1u;
        }
    }

    return res;
}

/**
 * Check if a value is multiple of another.
 * @param n     Value to check.
 * @param val   Value to check against.
 */
static bool posix_is_multiple (size_t n,
                               uint32_t val)
{
    bool res;

    res = (n % val) == 0 ? true : false;

    return res;
}

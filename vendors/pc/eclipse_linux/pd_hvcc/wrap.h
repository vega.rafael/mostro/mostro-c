/**
 * Brief description of the file.
 * A more detailed descriotion, which can span several lines.
 * Should be using JAVADOC_AUDOBRIEF set to YES.
 *
 * @file    module.hc
 * @author  JaaC
 * @version V0.0
 * @date    01/01/2016
 *
 * @par
 *
 * SMART TECHNOLOGY GROUP S.A.S. SHALL NOT BE HELD LIABLE FOR ANY DIRECT,
 * INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM
 * THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
 * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
 *
 * <h2><center>&copy; COPYRIGHT 2016 Smart Technology Group S.A.S. </center></h2>
 */

#ifndef _WRAP_H
#define _WRAP_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>
#include <stdint.h>

void *__wrap_malloc (size_t size);

void *__wrap_alloca (size_t size);

void *__wrap_realloc (void* ptr,
                      size_t size);

void __wrap_free (void* ptr);

uint32_t wrap_get_cnt (void);

uint32_t wrap_get_size_tot (void);


#ifdef __cplusplus
}
#endif

#endif /* _WRAP_H */

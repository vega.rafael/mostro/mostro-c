/**
 * Brief description of the file.
 * A more detailed descriotion, which can span several lines.
 * Should be using JAVADOC_AUDOBRIEF set to YES.
 *
 * @file    module.hc
 * @author  JaaC
 * @version V0.0
 * @date    01/01/2016
 *
 * @par
 *
 * SMART TECHNOLOGY GROUP S.A.S. SHALL NOT BE HELD LIABLE FOR ANY DIRECT,
 * INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM
 * THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
 * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
 *
 * <h2><center>&copy; COPYRIGHT 2016 Smart Technology Group S.A.S. </center></h2>
 */
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#define WRAP_CNT_ENTRIES (1000)

typedef struct
{
    void**  p_ptr;   //!< Pointer to array of pointers
    size_t* p_size;  //!< Pointer to array of sizes

} wrap_info_entry_t;

typedef struct
{
    wrap_info_entry_t* p_info;   //!< Entry information structure
    uint32_t           len;      //!< Length of array (number of entries)
    uint32_t           cnt;      //!< Index of current entry in array
    size_t             size_tot; //!< Totally used size

} wrap_info_array_t;

static void*             g_aray_ptr[WRAP_CNT_ENTRIES] = { NULL };
static uint32_t          g_array_size[WRAP_CNT_ENTRIES] = { 0 };

static wrap_info_entry_t g_array_entry = {
    .p_ptr  = g_aray_ptr,
    .p_size = g_array_size
};

static wrap_info_array_t g_array_info = {
    .p_info   = &g_array_entry,
    .len      = WRAP_CNT_ENTRIES,
    .cnt      =                0,
    .size_tot = 0
};

static bool wrap_find_slot (uint32_t* const p_slot);

static bool wrap_lookup_ptr (const void* const ptr,
                             uint32_t* const p_slot);

/**
 * A brief description of the function's purposes.
 * @param a A description for a.
 * @param b A description for b.
 * @return int8_t
 * @note An important note.
 * Detailed description of the function's purposes.
 */
void *__wrap_malloc (size_t size)
{
    void*    ptr = NULL;
    uint32_t idx;

    /* This is just to aid in debug */
    wrap_info_array_t* p_info = &g_array_info;

    /* Find a slot where to enter the pointer */
    if (wrap_find_slot(&idx))
    {
        ptr = __real_malloc(size);

        if (NULL != ptr)
        {
            /* Housekeeping */
            g_array_info.p_info->p_ptr[idx]  = ptr;
            g_array_info.p_info->p_size[idx] = size;
            g_array_info.size_tot           += size;
            g_array_info.cnt++;
        }
    }

    return ptr;
}

/**
 * A brief description of the function's purposes.
 * @param a A description for a.
 * @param b A description for b.
 * @return int8_t
 * @note An important note.
 * Detailed description of the function's purposes.
 */
void *__wrap_alloca (size_t size)
{
    void* ptr;

    /* This is just to aid in debug */
    wrap_info_array_t* p_info = &g_array_info;

    ptr = NULL;

    while (1)
    {}

    return ptr;
}

/**
 * A brief description of the function's purposes.
 * @param a A description for a.
 * @param b A description for b.
 * @return int8_t
 * @note An important note.
 * Detailed description of the function's purposes.
 */
void *__wrap_realloc (void* ptr,
                      size_t size)
{
    void*    ptr_new = NULL;
    uint32_t idx;

    /* This is just to aid in debug */
    wrap_info_array_t* p_info = &g_array_info;

    /* Lookup for the pointer */
    if (wrap_lookup_ptr(ptr, &idx))
    {
        ptr_new = __real_realloc(ptr, size);

        if (NULL != ptr_new)
        {
            /* Housekeeping */
            g_array_info.size_tot           -= g_array_info.p_info->p_size[idx];
            g_array_info.p_info->p_ptr[idx]  = ptr_new;
            g_array_info.p_info->p_size[idx] = size;
            g_array_info.size_tot           += size;
        }
    }

    return ptr_new;
}

/**
 * A brief description of the function's purposes.
 * @param a A description for a.
 * @param b A description for b.
 * @return int8_t
 * @note An important note.
 * Detailed description of the function's purposes.
 */
void __wrap_free (void* ptr)
{
    uint32_t idx;

    /* This is just to aid in debug */
    wrap_info_array_t* p_info = &g_array_info;

    /* Lookup for the pointer */
    if (wrap_lookup_ptr(ptr, &idx))
    {
        __real_free(ptr);

        /* Housekeeping */
        g_array_info.size_tot           -= g_array_info.p_info->p_size[idx];
        g_array_info.p_info->p_ptr[idx]  = NULL;
        g_array_info.p_info->p_size[idx] = 0;
        g_array_info.cnt--;
    }
}

/**
 * A brief description of the function's purposes.
 * @param a A description for a.
 * @param b A description for b.
 * @return int8_t
 * @note An important note.
 * Detailed description of the function's purposes.
 */
uint32_t wrap_get_cnt (void)
{
    return g_array_info.cnt;
}

/**
 * A brief description of the function's purposes.
 * @param a A description for a.
 * @param b A description for b.
 * @return int8_t
 * @note An important note.
 * Detailed description of the function's purposes.
 */
uint32_t wrap_get_size_tot (void)
{
    return g_array_info.size_tot;
}

/**
 * A brief description of the function's purposes.
 * @param a A description for a.
 * @param b A description for b.
 * @return int8_t
 * @note An important note.
 * Detailed description of the function's purposes.
 */
static bool wrap_find_slot (uint32_t* const p_slot)
{
    bool     res = false;
    uint32_t idx;

    if ((NULL != p_slot) && (g_array_info.cnt < g_array_info.len - 1u))
    {
        for (idx = 0 ; (!res) && (idx < g_array_info.len) ; idx++)
        {
            if (NULL == g_array_info.p_info->p_ptr[idx])
            {
                *p_slot = idx;
                res     = true;
            }
        }
    }

    return res;
}

/**
 * A brief description of the function's purposes.
 * @param a A description for a.
 * @param b A description for b.
 * @return int8_t
 * @note An important note.
 * Detailed description of the function's purposes.
 */
static bool wrap_lookup_ptr (const void* const ptr,
                             uint32_t* const p_slot)
{
    bool     res = false;
    uint32_t idx;

    if (NULL != p_slot)
    {
        for (idx = 0 ; (!res) && (idx < g_array_info.len) ; idx++)
        {
            if (ptr == g_array_info.p_info->p_ptr[idx])
            {
                *p_slot = idx;
                res     = true;
            }
        }
    }

    return res;
}
